# tanka-deployments

Kubernetes deployments at Gitlab using [tanka](https://tanka.dev/).

For Helmfile deployments see: [gitlab-helmfiles](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles) and [gitlab-com](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com)

[[_TOC_]]

## Getting started

See [Getting Started](./CONTRIBUTING.md#getting-started)

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md)

## Maintainers

See [MAINTAINERS.md](./MAINTAINERS.md)

local clusters = import 'gitlab/clusters.libsonnet';
local externalSecrets = import 'gitlab/external-secrets.libsonnet';

{
  new(env, namespace, appName, config={})::
    local _config = { clusters: {} } + config;

    local
      labels(cluster_name) = {
        'app.kubernetes.io/name': 'redis',
        'app.kubernetes.io/instance': cluster_name,
      },
      annotations = {
        'kubernetes.io/service-account.name': appName,
      };

    local secretKey(name) = externalSecrets.k8sEnvSecretKey(env, name);

    {
      [cluster.name]: {
        redis_password:
          externalSecrets.externalSecret(
            '%s-password' % cluster.name,
            appName,
            namespace,
            clusters[env].clusterName,
            labels=labels(cluster.name),
            secretAnnotations=annotations,
            secretStorePath='shared',
            data={
              'redis-password': {
                key: secretKey('redis/%s' % cluster.name),
                property: 'password',
                version: 1,
              },
            },
          ),
      }
      for cluster in _config.clusters
    },
}

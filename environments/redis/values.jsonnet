local clusters = {
  registryCache: {
    name: 'redis-registry-cache',
    externalMaster: { enabled: false },
    replicaCount: 3,
    redisResourcesMemory: '16Gi',
    specialConfiguration: |||
      save 900 1
      maxmemory 8gb
      maxmemory-policy allkeys-lru
      maxmemory-samples 5
    |||,
    storageClass: 'pd-balanced',
    dataStorageSize: '20Gi',
  },
};

{
  pre: {
    clusters: std.objectValues(clusters),
  },
  'pre-2': {
    clusters: std.objectValues(clusters),
  },
  gstg: {
    clusters: std.objectValues(clusters),
  },
  gprd: {
    clusters: std.objectValues(clusters),
  },
  dev: {
    local devResources = {
      redis: { memory: '1Gi', cpu: 0.5 },
      sentinel: { memory: '0.5Gi', cpu: 0.5 },
      exporter: { memory: '0.25Gi', cpu: 0.25 },
      metrics: { memory: '0.25Gi', cpu: 0.25 },
    },
    clusters: std.map(function(cluster) cluster { resources: devResources, storageClass: '', dataStorageSize: '0.5Gi' }, std.objectValues(clusters)),
  },
}

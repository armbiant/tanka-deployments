local common = import 'gitlab/common.libsonnet';
local redis = import 'redis/redis.libsonnet';
local secrets = import 'secrets.jsonnet';
local values = import 'values.jsonnet';

{
  local appName = 'redis',
  local namespaceName = 'redis',
  local defaultEnvData = {
    namespaceName:: namespaceName,
    createNamespace:: true,
  },
  local gkeEnvData(env) = defaultEnvData {
    externalSecretsRole:: appName,
    secretStorePaths:: ['shared'],
    redis: {
      [cluster.name]: redis.new(
        env,
        appName,
        cluster,
        nodeSelector={ type: cluster.name },
        tolerations=[
          {
            key: 'gitlab.com/type',
            value: cluster.name,
            effect: 'NoSchedule',
          },
        ],
        zoneAntiAffinity=true
      )
      for cluster in values[env].clusters
    },
    redis_secrets: secrets.new(env, namespaceName, appName, values[env]),
  },
  envsData:: {
    dev: defaultEnvData {
      redis: {
        [cluster.name]: redis.new('dev', appName, cluster)
        for cluster in values.dev.clusters
      },
    },
    pre: gkeEnvData('pre'),
    'pre-2': gkeEnvData('pre-2'),
    gstg: gkeEnvData('gstg'),
    gprd: gkeEnvData('gprd'),
  },

  envs: common.genGitlabEnvs(appName, $.envsData),
}

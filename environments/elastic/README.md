# elastic

deployment steps:
```
$ tk apply environments/es-poc --name "environments/es-poc/dev"
```

access Kibana in your browser:
```
$ kubectl port-forward -n es-poc service/kibana-kibana 5601
```
and go to `localhost:5601` in your browser

local environments = import 'environments.libsonnet';
local common = import 'gitlab/common.libsonnet';
local k = import 'ksonnet-util/kausal.libsonnet';

local container = k.core.v1.container;
local port = k.core.v1.containerPort;
local cronJob = k.batch.v1.cronJob;

local probe = {
  httpGet: {
    path: '/ready',
    port: 'http',
  },
};

{
  new(env, namespace, appName, tag):: {
    namespace: common.namespace(namespace),
    serveDeployment: $.serveDeployment(env, appName, tag),
    service: $.serviceFor(self.serveDeployment),
    serviceMonitor: $.serviceMonitor(env, appName),
    notifyExpiringSilencesCronJob: $.notifyExpiringSilencesCronJob(env, appName, tag),
    handoverCronJob: $.handoverCronJob(env, appName, tag),
    archiveIncidentChannelsCronJob: $.archiveIncidentChannelsCronJob(env, appName, tag),
    updateOncallUsergroupsDeployments: $.updateOncallUsergroupsDeployments(env, appName, tag),
  },

  configDefaults(appName):: {
    image: 'registry.gitlab.com/gitlab-com/gl-infra/woodhouse',
    secretName: appName,
    adminSecretName: '%s-admin' % appName,
  },

  deployment(env, appName, tag, replicas)::
    local envConfig = $.configDefaults(appName) + environments[env];
    local config = {
      LOG_FORMAT: 'json',
    } + envConfig.config;

    common.deployment(
      name=appName,
      type='woodhouse',
      replicas=replicas,
      containers=[],
    ) + {
      metadata+: {
        labels+: {
          name: appName,
          type: 'woodhouse',
          tier: 'sv',
          stage: 'main',
          shard: 'default',
        },
      },
      spec+: {
        template+: {
          metadata+: {
            labels+: {
              type: 'woodhouse',
              tier: 'sv',
              stage: 'main',
              shard: 'default',
            },
          },
          spec+: (
            if std.objectHas(envConfig, 'nodePool') then
              common.withNodePoolAffinity(envConfig.nodePool)
            else {}
          ),
        },
      },
    },

  serveDeployment(env, appName, tag)::
    local envConfig = $.configDefaults(appName) + environments[env];
    local config = {
      LOG_FORMAT: 'json',
    } + envConfig.config;


    $.deployment(env, appName, tag, envConfig.replicas) + {
      spec+: {
        template+: {
          spec+: {
            containers+: [
              container.new(name='woodhouse', image='%s:%s' % [envConfig.image, tag])
              + container.withPorts([port.new('http', 8080), port.new('metrics', 8081)])
              + container.withEnvMap(config)
              + {
                // Take imagePullPolicy private to remove it from the final object.
                // We want to rely on the default behaviour of imagePullPolicy to
                // avoid using stale `:latest` images.
                imagePullPolicy:: super.imagePullPolicy,

                command: ['woodhouse', 'serve'],
                envFrom: [
                  {
                    secretRef: { name: envConfig.secretName },
                  },
                ],
                livenessProbe: probe,
                readinessProbe: probe,
                resources: envConfig.resources,
              },
            ],
          },
        },
      },
    },

  updateOncallUsergroupsDeployments(env, appName, tag)::
    if std.objectHas(environments[env], 'updateOncallUsergroups') then
      [$.updateOncallUsergroupsDeployment(env, appName, tag, envKey) for envKey in std.objectFields(environments[env].updateOncallUsergroups)]
    else {},

  updateOncallUsergroupsDeployment(env, appName, tag, envKey)::
    local envConfig = $.configDefaults(appName) + environments[env];
    local config = {
      LOG_FORMAT: 'json',
    } + envConfig.config;

    local deployConfig = envConfig.updateOncallUsergroups[envKey];

    if deployConfig.enabled then
      $.deployment(env, '%s-update-oncall-usergroups-%s' % [appName, envKey], tag, 1) + {
        spec+: {
          template+: {
            spec+: {
              containers+: [
                container.new(name='woodhouse', image='%s:%s' % [envConfig.image, tag])
                + container.withPorts([])
                + container.withEnvMap(config + deployConfig.config)
                + {
                  imagePullPolicy:: super.imagePullPolicy,
                  command: ['woodhouse', 'slack', 'update-oncall-usergroups'],
                  envFrom: [
                    {
                      secretRef: { name: envConfig.secretName },
                    },
                    {
                      secretRef: { name: envConfig.adminSecretName },
                    },
                  ],
                  resources: deployConfig.resources,
                },
              ],
            },
          },
        },
      }
    else {},

  serviceFor(deployment)::
    k.util.serviceFor(deployment) + {
      spec+: {
        type: 'NodePort',
      },
    },

  serviceMonitor(env, appName):: {
    apiVersion: 'monitoring.coreos.com/v1',
    kind: 'ServiceMonitor',
    metadata: {
      name: appName,
      labels: {
        'app.kubernetes.io/component': 'metrics',
        'app.kubernetes.io/instance': appName,
        'app.kubernetes.io/name': 'woodhouse',
        'gitlab.com/prometheus-instance': 'prometheus-system',
      },
    },
    spec: {
      endpoints: [
        {
          interval: '15s',
          path: '/metrics',
          port: 'woodhouse-metrics',
          relabelings: [
            {
              targetLabel: 'type',
              replacement: 'woodhouse',
            },
            {
              targetLabel: 'tier',
              replacement: 'sv',
            },
            {
              targetLabel: 'stage',
              replacement: 'main',
            },
            {
              targetLabel: 'shard',
              replacement: 'default',
            },
            {
              targetLabel: 'environment',
              replacement: env,
            },
          ],
        },
      ],
      selector: {
        matchLabels: {
          name: appName,
        },
      },
    },
  },

  ingress(env, appName)::
    local envConfig = environments[env];
    {
      ingress: {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'Ingress',
        metadata: {
          name: appName,
          annotations: {
            'kubernetes.io/ingress.global-static-ip-name': envConfig.ingress.staticIPName,
            'networking.gke.io/managed-certificates': appName,
          },
          labels: {
            type: 'woodhouse',
            tier: 'sv',
            stage: 'main',
            shard: 'default',
          },
        },
        spec: {
          rules: [
            {
              host: envConfig.ingress.domain,
              http: {
                paths: [
                  {
                    backend: {
                      service: {
                        name: appName,
                        port: {
                          name: 'woodhouse-http',
                        },
                      },
                    },
                    path: '/*',
                    pathType: 'ImplementationSpecific',
                  },
                ],
              },
            },
          ],
        },
      },
      managedCert: common.managedCert(appName, [envConfig.ingress.domain]),
    },

  notifyExpiringSilencesCronJob(env, appName, tag)::
    $.cronJob(
      env,
      appName,
      tag,
      envKey='notifyExpiringSilences',
      jobName='%s-notify-expiring-silences' % appName,
      cmd=['woodhouse', 'alertmanager', 'notify-expiring-silences'],
    ),

  handoverCronJob(env, appName, tag)::
    $.cronJob(
      env,
      appName,
      tag,
      envKey='handover',
      jobName='%s-handover' % appName,
      cmd=['woodhouse', 'handover'],
    ),

  archiveIncidentChannelsCronJob(env, appName, tag)::
    $.cronJob(
      env,
      appName,
      tag,
      envKey='archiveIncidentChannels',
      jobName='%s-archive-incident-channels' % appName,
      cmd=['woodhouse', 'slack', 'archive-incident-channels'],
    ),

  cronJob(env, appName, tag, envKey, jobName, cmd)::
    local envConfig = $.configDefaults(appName) + environments[env];
    local config = {
      LOG_FORMAT: 'json',
    } + envConfig.config;

    if std.objectHas(envConfig, envKey) && envConfig[envKey].enabled then
      cronJob.new(
        name=jobName,
        schedule=envConfig[envKey].schedule,
        containers=[
          container.new(name='woodhouse', image='%s:%s' % [envConfig.image, tag])
          + container.withEnvMap(config + (if std.objectHas(envConfig[envKey], 'config') then envConfig[envKey].config else {}))
          + {
            imagePullPolicy:: super.imagePullPolicy,

            command: cmd,
            envFrom: [
              {
                secretRef: { name: envConfig.secretName },
              },
            ],
            resources: envConfig[envKey].resources,
          },
        ],
      )
      + cronJob.mixin.spec.jobTemplate.spec.template.spec.withRestartPolicy('Never')
      + {
        metadata+: {
          labels+: {
            name: jobName,
          },
        },
      }
    else
      {},
}

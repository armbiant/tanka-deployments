# Woodhouse

https://gitlab.com/gitlab-com/gl-infra/woodhouse

## Deployment

Woodhouse can take either a flag or an environment variable for each of its
configuration elements. See
[Woodhouse's readme](https://gitlab.com/gitlab-com/gl-infra/woodhouse) for
information on that config.

The secrets are [managed in Vault](https://vault.gitlab.net/ui/vault/secrets/k8s/list/ops-gitlab-gke/delivery/)
and created in Kubernetes by the External Secrets operator.

Woodhouse is slightly different from most of our deployments, in that it depends
on an external variable - the image tag. In CI, this is defaulted to latest, and
overridden by API-triggered pipelines from Woodhouse's own pipelines, but in dev
you'll need to set it:

```shell
tk diff --with-prune environments/woodhouse --name woodhouse/dev --tla-str tag=latest
```

local podResources = {
  requests: {
    cpu: '1',
    memory: '256Mi',
  },
  limits: {
    memory: '256Mi',
  },
};

{
  ops: {
    config: {
      GITLAB_INCIDENT_ISSUE_PROJECT_PATH: 'gitlab-com/gl-infra/production',
      GITLAB_PRODUCTION_PROJECT_PATH: 'gitlab-com/gl-infra/production',
      INCIDENT_SLACK_CHANNEL: 'CB7P5CJS1',  // incident-management
      CHANGE_SLACK_CHANNEL: 'C101F3796',  // production
      SHUTDOWN_SLEEP_SECONDS: '10',
      MAX_REQUESTS_PER_SECOND_PER_IP: '20',
      INCIDENT_CHANNEL_NAME_PREFIX: 'incident',
      PAGERDUTY_ESCALATION_POLICY_EOC: 'P7IG7DS',
      PAGERDUTY_ESCALATION_POLICY_IMOC: 'P3N1TU2',
      PAGERDUTY_ESCALATION_POLICY_CMOC: 'PNH1Z1L',
      STATUSIO_PAGE_ID: '5b36dc6502d06804c08349f7',
      STATUSIO_PAGE_BASE_URL: 'https://status.gitlab.com',
    },
    secretName: 'woodhouse-v1',
    adminSecretName: 'woodhouse-admin-v1',
    replicas: 2,
    resources: podResources,
    ingress: {
      domain: 'woodhouse.ops.gitlab.net',
      staticIPName: 'woodhouse-ops',
    },
    notifyExpiringSilences: {
      enabled: true,
      config: {
        ALERTMANATER_SLACK_CHANNEL: 'C101F3796',  // production
        ALERTMANAGER_BASE_URL: 'http://alertmanager-headless.monitoring.svc.cluster.local:9093',
        ALERTMANAGER_EXTERNAL_BASE_URL: 'https://alerts.gitlab.net',
        SILENCE_END_CUTOFF: '8h',
      },
      // run at 1 minute after each 8-hour SRE shift starts
      // note: this may need to be adjusted on DST change
      // starting with Kubernetes v1.22 it will be possible
      // to specify a timezone in the schedule, see:
      // https://github.com/kubernetes/kubernetes/issues/47202
      schedule: '1 7,15,23 * * *',
      resources: podResources,
    },
    handover: {
      enabled: true,
      config: {
        GITLAB_HANDOVER_PROJECT_PATH: 'gitlab-com/gl-infra/on-call-handovers',
        PAGERDUTY_SERVICE_IDS: 'PATDFCE',
      },
      // run at 59 minutes before each 8-hour SRE shift ends
      schedule: '1 6,14,22 * * *',
      resources: {
        requests: {
          cpu: '1',
          memory: '256Mi',
        },
        limits: {
          memory: '256Mi',
        },
      },
    },
    archiveIncidentChannels: {
      enabled: true,
      // daily
      schedule: '19 11 * * *',
      resources: {
        requests: {
          cpu: '1',
          memory: '256Mi',
        },
        limits: {
          memory: '256Mi',
        },
      },
    },
    updateOncallUsergroups: {
      eoc: {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'P7IG7DS',
          SLACK_GROUP_ID: 'SRXBY6SSC',
        },
        resources: podResources,
      },
      'incident-managers': {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'P3N1TU2',
          SLACK_GROUP_ID: 'S046PL9BCS1',
        },
        resources: podResources,
      },
      cmoc: {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'PNH1Z1L',
          SLACK_GROUP_ID: 'S046SGQ3GRH',
        },
        resources: podResources,
      },
    },
  },

  gstg: {
    config: {
      GITLAB_INCIDENT_ISSUE_PROJECT_PATH: 'gitlab-com/gl-infra/woodhouse-integration-test',
      GITLAB_PRODUCTION_PROJECT_PATH: 'gitlab-com/gl-infra/woodhouse-integration-test',
      INCIDENT_SLACK_CHANNEL: 'C01CB7PNR0R',  // woodhouse-staging
      CHANGE_SLACK_CHANNEL: 'C01CB7PNR0R',  // woodhouse-staging
      GLOBAL_SLASH_COMMAND: 'woodhouse-staging',
      STATUSIO_PAGE_ID: '5bedc0c2a394fc04c9ccc974',
      INCIDENT_CHANNEL_NAME_PREFIX: 'woodhouse-integration-test',
    },
    secretName: 'woodhouse-v1',
    adminSecretName: 'woodhouse-admin-v1',
    replicas: 1,
    resources: podResources,
    ingress: {
      domain: 'woodhouse.gstg.gitlab.net',
      staticIPName: 'woodhouse-gstg',
    },
    archiveIncidentChannels: {
      enabled: true,
      // daily
      schedule: '19 11 * * *',
      resources: podResources,
    },
  },

  dev: {
    config: {
      GITLAB_INCIDENT_ISSUE_PROJECT_PATH: 'dummy',
      GITLAB_PRODUCTION_PROJECT_PATH: 'dummy',
      INCIDENT_SLACK_CHANNEL: 'dummy',
      LOG_FORMAT: 'logfmt',
    },
    replicas: 1,
    resources: {},
  },
}

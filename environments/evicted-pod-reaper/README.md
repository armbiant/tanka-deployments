## Evicted Pod Reaper
This app is a simple Kubernetes `cronjob` that runs every 30 minutes
to look inside the `gitlab` namespace for any evicted pods and remove
them. This is because Kubernetes does no pruning of these pods themselves,
and it cases some of our alerts to fire when there is no actual issue.

The original issue where this solution was discussed is at

https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1126

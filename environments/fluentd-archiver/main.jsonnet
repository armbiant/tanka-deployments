local fluentdArchiver = import 'fluentd/archiver.libsonnet';
local common = import 'gitlab/common.libsonnet';
local secrets = import 'secrets.jsonnet';
local values = import 'values.jsonnet';

{
  local appName = 'fluentd-archiver',
  local namespaceName = 'logging',

  envsData:: {
    gprd: {
      namespaceName:: namespaceName,
      createNamespace:: false,
      externalSecretsRole:: 'fluentd',
      secretStorePaths:: ['shared'],
      fluentdArchiver: fluentdArchiver.new('gprd', namespaceName, config=values.gprd),
      fluentdSecrets: secrets.new('gprd', namespaceName, appName),
    },
    gstg: {
      namespaceName:: namespaceName,
      createNamespace:: false,
      externalSecretsRole:: 'fluentd',
      secretStorePaths:: ['shared'],
      fluentdArchiver: fluentdArchiver.new('gstg', namespaceName, config=values.gstg),
      fluentdSecrets: secrets.new('gstg', namespaceName, appName),
    },
    ops: {
      namespaceName:: namespaceName,
      createNamespace:: false,
      externalSecretsRole:: 'fluentd',
      secretStorePaths:: ['shared'],
      fluentdArchiver: fluentdArchiver.new('ops', namespaceName, config=values.ops),
      fluentdSecrets: secrets.new('ops', namespaceName, appName),
    },
    pre: {
      namespaceName:: namespaceName,
      createNamespace:: false,
      externalSecretsRole:: 'fluentd',
      secretStorePaths:: ['shared'],
      fluentdArchiver: fluentdArchiver.new('pre', namespaceName, config=values.pre),
      fluentdSecrets: secrets.new('pre', namespaceName, appName),
    },
    dev: {
      namespaceName:: namespaceName,
      createNamespace:: false,
      archiver: fluentdArchiver.new('dev', namespaceName, config=values.dev),
    },
  },

  envs: common.genGitlabEnvs(appName, $.envsData),
}

local pubsubTopics = import 'gitlab/pubsub-topics.libsonnet';

local defaultConfig = {
  name: 'dev',
  imagePullSecrets+: ['fluentd-archiver-image-pull-secret'],
  googleServiceAccount: 'fluentd-archiver-k8s@gitlab-dev.iam.gserviceaccount.com',
  pubsub+: {
    topics: pubsubTopics.dev,
  },
  stageLabel: 'main',
};

{
  gprd: defaultConfig {
    name: 'gprd',
    gcs+: {
      flushInterval: '2m',
    },
    googleServiceAccount: 'fluentd-archiver-k8s@gitlab-production.iam.gserviceaccount.com',
    pubsub+: {
      max_messages: 1000,
      pull_interval: 4.0,
      topics: pubsubTopics.gprd,
    },
    replicas: 60,
  },
  gstg: defaultConfig {
    name: 'gstg',
    googleServiceAccount: 'fluentd-archiver-k8s@gitlab-staging-1.iam.gserviceaccount.com',
    pubsub+: {
      topics: pubsubTopics.gstg,
    },
    replicas: 4,
  },
  ops: defaultConfig {
    name: 'ops',
    googleServiceAccount: 'fluentd-archiver-k8s@gitlab-ops.iam.gserviceaccount.com',
    pubsub+: {
      topics: pubsubTopics.ops,
    },
    replicas: 4,
  },
  pre: defaultConfig {
    name: 'pre',
    googleServiceAccount: 'fluentd-archiver-k8s@gitlab-pre.iam.gserviceaccount.com',
    pubsub+: {
      topics: pubsubTopics.pre,
    },
    replicas: 2,
  },
  dev: defaultConfig,
}

local clusters = import 'gitlab/clusters.libsonnet';

{
  ops: {
    googleProject: clusters.ops.googleProject,
    resources: {
      requests: {
        cpu: '1',
        memory: '128Mi',
      },
      limits: {
        memory: '128Mi',
      },
    },
  },
  pre: {
    googleProject: clusters.pre.googleProject,
  },
  gstg: {
    googleProject: clusters.gstg.googleProject,
  },
  gprd: {
    googleProject: clusters.gprd.googleProject,
  },
  dev: {},
}

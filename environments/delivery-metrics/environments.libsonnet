{
  ops: {
    config: {},
    replicas: 1,
    resources: {
      requests: {
        cpu: '1',
        memory: '128Mi',
      },
      limits: {
        memory: '128Mi',
      },
    },
    ingress: {
      domain: 'delivery-metrics.ops.gitlab.net',
      staticIPName: 'delivery-metrics-ops',
    },
    secretName: 'delivery-metrics-v1',
    imagePullSecrets: ['delivery-metrics-image-pull-secret-v1'],
  },

  dev: {},
}

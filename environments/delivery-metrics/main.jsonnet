local delivery_metrics = import 'delivery-metrics.libsonnet';
local environments = import 'environments.libsonnet';
local common = import 'gitlab/common.libsonnet';
local secrets = import 'secrets.libsonnet';

local appName = 'delivery-metrics';
local namespaceName = 'delivery';

function(tag='latest') {
  envsData:: {
    [env]: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      externalSecretsRole:: 'delivery-metrics',
      deliveryMetrics:
        delivery_metrics.new(env, namespaceName, appName, tag)
        +
        if std.objectHas(environments[env], 'ingress') then
          delivery_metrics.ingress(env, appName)
        else {},
      deliverySecrets: secrets.new(env, namespaceName, appName),
    }
    for env in std.objectFields(environments)
  },

  envs: common.genGitlabEnvs(appName, $.envsData),
}

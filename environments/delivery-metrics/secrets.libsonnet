local clusters = import 'gitlab/clusters.libsonnet';
local externalSecrets = import 'gitlab/external-secrets.libsonnet';

{
  new(env, namespaceName, appName):: {
    local labels = {
      'app.kubernetes.io/instance': appName,
      'app.kubernetes.io/name': 'delivery-metrics',
    },
    local clusterName = clusters[env].clusterName,

    local secretKey(name) = externalSecrets.k8sNamespaceSecretKey(clusterName, namespaceName, name),

    appSecrets:
      externalSecrets.externalSecret(
        '%s-v1' % appName,
        appName,
        namespaceName,
        clusterName,
        labels=labels,
        data={
          AUTH_TOKEN: {
            key: secretKey('delivery-metrics'),
            property: 'auth_token',
            version: 1,
          },
          DELIVERY_METRICS_OPS_TOKEN: {
            key: secretKey('gitlab'),
            property: 'api_token',
            version: 1,
          },
          JOB_WEBHOOK_DEPLOYER_OPS_TOKEN: {
            key: secretKey('delivery-metrics-webhooks/job-webhook-deployer-ops'),
            property: 'webhook_token',
            version: 2,
          },
          JOB_WEBHOOK_RELEASE_TOOLS_OPS_TOKEN: {
            key: secretKey('delivery-metrics-webhooks/job-webhook-release-tools-ops'),
            property: 'webhook_token',
            version: 2,
          },
          JOB_WEBHOOK_QUALITY_STAGING_CANARY_OPS_TOKEN: {
            key: secretKey('delivery-metrics-webhooks/job-webhook-quality-staging-canary-ops'),
            property: 'webhook_token',
            version: 1,
          },
          JOB_WEBHOOK_QUALITY_STAGING_OPS_TOKEN: {
            key: secretKey('delivery-metrics-webhooks/job-webhook-quality-staging-ops'),
            property: 'webhook_token',
            version: 1,
          },
          JOB_WEBHOOK_K8S_WORKLOADS_GITLAB_COM_OPS_TOKEN: {
            key: secretKey('delivery-metrics-webhooks/job-webhook-k8s-workloads-gitlab-com-ops'),
            property: 'webhook_token',
            version: 1,
          },
          JOB_WEBHOOK_CNG_DEV_TOKEN: {
            key: secretKey('delivery-metrics-webhooks/job-webhook-charts-components-images-dev'),
            property: 'webhook_token',
            version: 1,
          },
          JOB_WEBHOOK_OMNIBUS_DEV_TOKEN: {
            key: secretKey('delivery-metrics-webhooks/job-webhook-omnibus-dev'),
            property: 'webhook_token',
            version: 1,
          },
        },
      ),

    imagePullSecret:
      externalSecrets.externalSecret(
        '%s-image-pull-secret-v1' % appName,
        appName,
        namespaceName,
        clusterName,
        labels=labels,
        secretType='kubernetes.io/dockerconfigjson',
        secretDataTemplate={
          '.dockerconfigjson': |||
            {"auths": {"registry.ops.gitlab.net": {"auth": "{{ (printf "%s:%s" .username .password) | b64enc }}"}}}
          |||,
        },
        data={
          username: {
            key: secretKey('registry-ops'),
            property: 'username',
            version: 1,
          },
          password: {
            key: secretKey('registry-ops'),
            property: 'password',
            version: 1,
          },
        },
      ),
  },
}

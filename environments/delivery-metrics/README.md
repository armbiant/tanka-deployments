# Delivery Metrics

https://gitlab.com/gitlab-org/release-tools/

## Deployment

`delivery-metrics` needs a couple environment variables named `AUTH_TOKEN` and
`DELIVERY_METRICS_OPS_TOKEN`, those secrets are
[managed in Vault](https://vault.gitlab.net/ui/vault/secrets/k8s/list/ops-gitlab-gke/delivery/)
and created in Kubernetes by the External Secrets operator.

`delivery-metrics` is slightly different from most of our deployments, in that it depends
on an external variable - the image tag. In CI, this is defaulted to latest, and
overridden by API-triggered pipelines from `release-tools`' own pipelines, but in dev
you'll need to set it:

```shell
tk diff --with-prune environments/delivery-metrics --name delivery-metrics/dev --tla-str tag=latest
```

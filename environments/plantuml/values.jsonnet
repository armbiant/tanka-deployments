{
  gprd: {
    name: 'gprd',
    domains: [
      'plantuml.gitlab-static.net',
    ],
    plantumlImage: 'plantuml/plantuml-server:jetty-v1.2022.13',
    resources: {
      requests: { cpu: '500m', memory: '3G' },
      limits: { cpu: 1, memory: '4G' },
    },
    server_name: 'plantuml.gitlab-static.net',
    redirect_url: 'https://gitlab.com',
  },
  gstg: {
    name: 'gstg',
    domains: [
      'gstg.plantuml.gitlab-static.net',
    ],
    plantumlImage: 'plantuml/plantuml-server:jetty-v1.2022.13',
    server_name: 'gstg.plantuml.gitlab-static.net',
    redirect_url: 'https://staging.gitlab.com',
  },
  pre: {
    name: 'pre',
    domains: [
      'pre.plantuml.gitlab-static.net',
    ],
    plantumlImage: 'plantuml/plantuml-server:jetty-v1.2022.13',
    server_name: 'pre.plantuml.gitlab-static.net',
    redirect_url: 'https://pre.gitlab.com',
  },
  dev: {
    name: 'dev',
  },
}

local common = import 'gitlab/common.libsonnet';

local defaultStoreShardResources = {
  requests: {
    cpu: '1',
    memory: '5Gi',
  },
  limits: {
    memory: '5Gi',
  },
};

local defaultStoreShardDiskSize = '5Gi';
local defaultStorageClass = 'ssd';

local storeMemcached(
  indexReplicas=1,
  indexCPU='600m',
  indexCPULimit='2000m',
  indexMemory='2Gi',
  indexMemoryLimit='4Gi',
  indexMaxConnections='4096',
  bucketReplicas=1,
  bucketCPU='600m',
  bucketCPULimit='2000m',
  bucketMemory='2Gi',
  bucketMemoryLimit='4Gi',
  bucketMaxConnections='4096',
      ) = {
  indexMemcached: {
    values: {
      resources: {
        requests: {
          cpu: indexCPU,
          memory: indexMemory,
        },
        limits: {
          cpu: indexCPULimit,
          memory: indexMemoryLimit,
        },
      },
      replicaCount: indexReplicas,
      extraEnvVars+: {
        MEMCACHED_MAX_CONNECTIONS: indexMaxConnections,
      },
    },
  },
  bucketMemcached: {
    values: {
      resources: {
        requests: {
          cpu: bucketCPU,
          memory: bucketMemory,
        },
        limits: {
          cpu: bucketCPULimit,
          memory: bucketMemoryLimit,
        },
      },
      replicaCount: bucketReplicas,
      extraEnvVars+: {
        MEMCACHED_MAX_CONNECTIONS: bucketMaxConnections,
      },
    },
  },
};

local stackdriverTracing = {
  type: 'GOOGLE_CLOUD',
  config: {
    project_id: 'gitlab-ops',
    sample_factor: 16,
  },
};

local defaultConfig = {
  name: 'dev',
  type: 'thanos',
};

{
  gprd: defaultConfig {
    name: 'gprd',
    store+: {
      bucketName: 'gitlab-gprd-prometheus',
      config+: {
        shards: 24,
        replicas: 2,
        resources: {
          requests: {
            cpu: '2000m',
            memory: '20Gi',
          },
          limits: {
            memory: '48Gi',
            cpu: '8000m',
          },
        },
        tracing: stackdriverTracing,
        extraFlags: [
          '--chunk-pool-size=10GB',
          '--store.grpc.series-max-concurrency=60',
          '--block-meta-fetch-concurrency=128',
        ],
      },
      diskSize: '100Gi',
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(
      indexReplicas=3,
      indexCPU='3000m',
      indexCPULimit='6000m',
      indexMemory='40Gi',
      indexMemoryLimit='50Gi',
      indexMaxConnections='12288',
      bucketReplicas=3,
      bucketCPU='1000m',
      bucketCPULimit='3000m',
      bucketMemory='15Gi',
      bucketMemoryLimit='20Gi',
      bucketMaxConnections='12288',
    ),
  },

  gstg: defaultConfig {
    name: 'gstg',
    store+: {
      bucketName: 'gitlab-gstg-prometheus',
      config+: {
        shards: 3,
        replicas: 2,
        resources: {
          requests: {
            cpu: '1000m',
            memory: '8Gi',
          },
          limits: {
            memory: '16Gi',
          },
        },
        tracing: stackdriverTracing,
      },
      diskSize: '45Gi',
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(),
  },

  ops: defaultConfig {
    name: 'ops',
    store+: {
      bucketName: 'gitlab-ops-prometheus',
      config+: {
        shards: 1,
        replicas: 2,
        resources: defaultStoreShardResources,
        tracing: stackdriverTracing,
      },
      diskSize: '20Gi',
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(
      indexReplicas=3,
      bucketReplicas=3,
      bucketCPU='1200m',
      bucketCPULimit='3000m',
      bucketMemory='6Gi',
      bucketMemoryLimit='10Gi',
    ),
    query+: {
      config+: {
        replicas: 5,
        resources: {
          requests: {
            cpu: '5000m',
            memory: '16Gi',
          },
          limits: {
            cpu: '5000m',
            memory: '32Gi',
          },
        },
        tracing: stackdriverTracing,
        // We can use dns (via external-dns) to discover GKE-deployed prometheus
        // and thanos-store pods.  For GCE-deployed endpoints, we have a
        // problem: we can't use `.internal` URLs outside of the GCP project the
        // URL refers to. We could use static IPs, or create DNS entries for the
        // private IPs of these endpoints.
        stores: [
          'dns+%s:10901' % common.externalURL('thanos-store-internal-%d' % i, env)
          for env in common.clusters.regionals()
          if env != 'ops-stg' && env != 'pre-2'
          for i in std.range(0, $[env].store.config.shards - 1)
        ] + [
          // Prometheus resources are currently managed by helm, outside of this
          // repo. When we move this to tanka, we might want to move this part
          // of the function to encapsulate prometheus concerns in one place.
          'dns+%s:10901' % common.externalURL('prometheus-internal', env)
          for env in common.clusters.all()
          if env != 'ops-stg' && env != 'pre-2'
        ] + [
          // GCE prometheus private IPs.
          // This is not the most robust solution, and we should replace it with
          // DNS service discovery, but we're likely to move these jobs to
          // kube-deployed prometheus before the effort paid into DNS discovery
          // would be paid off.
          //
          // knife search 'recipes:gitlab-prometheus\:\:thanos AND cloud_provider:gce AND (thanos-rule_enable:true OR thanos-sidecar_enable:true)' -F json -a hostname -a ipaddress | jq -r ".rows[] | to_entries[] | \"  '\" + .value.ipaddress + \":10901',  // \" + .key" | sort -u
          '10.219.1.10:10901',  // prometheus-01-inf-gprd.c.gitlab-production.internal
          '10.219.1.11:10901',  // prometheus-db-02-inf-gprd.c.gitlab-production.internal
          '10.219.1.4:10901',  // prometheus-app-01-inf-gprd.c.gitlab-production.internal
          '10.219.1.5:10901',  // prometheus-app-02-inf-gprd.c.gitlab-production.internal
          '10.219.1.8:10901',  // prometheus-db-01-inf-gprd.c.gitlab-production.internal
          '10.219.1.9:10901',  // prometheus-02-inf-gprd.c.gitlab-production.internal
          '10.226.1.22:10901',  // prometheus-01-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.23:10901',  // prometheus-app-01-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.24:10901',  // prometheus-db-01-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.25:10901',  // prometheus-02-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.26:10901',  // prometheus-app-02-inf-gstg.c.gitlab-staging-1.internal
          '10.226.1.27:10901',  // prometheus-db-02-inf-gstg.c.gitlab-staging-1.internal
          '10.232.3.15:10901',  // prometheus-app-01-inf-pre.c.gitlab-pre.internal
          '10.232.3.17:10901',  // prometheus-01-inf-pre.c.gitlab-pre.internal
          '10.240.3.3:10901',  // prometheus-01-inf-testbed.c.gitlab-testbed.internal
          '10.250.25.101:10901',  // thanos-rule-01-inf-ops.c.gitlab-ops.internal
          '10.250.25.102:10901',  // thanos-rule-02-inf-ops.c.gitlab-ops.internal
          '10.250.8.16:10901',  // prometheus-02-inf-ops.c.gitlab-ops.internal
          '10.250.8.4:10901',  // prometheus-01-inf-ops.c.gitlab-ops.internal
          'dns+prometheus-db-thanos.db-benchmarking.gitlab.com:10901',  // prometheus-db-01-inf-db-benchmarking.c.gitlab-db-benchmarking.internal
          'dns+prometheus-thanos.db-benchmarking.gitlab.com:10901',  // prometheus-01-inf-db-benchmarking.c.gitlab-db-benchmarking.internal
          'dns+prometheus.staging-ref.gitlab.com:10901',
          'thanos-query.opstracegcp.com:80',  // thanos stores for observability platform (ref: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16395)
        ],
      },
    },
    queryFrontend+: {
      config+: {
        logQueriesLongerThan: '30s',
        replicas: 3,
        resources: {
          requests: {
            cpu: '2000m',
            memory: '6Gi',
          },
          limits: {
            cpu: '2000m',
            memory: '6Gi',
          },
        },
        tracing: stackdriverTracing,
        splitInterval: '6h',
      },
      queryRangeMemcached: {
        values: {
          replicaCount: 4,
          resources: {
            requests: {
              cpu: '1000m',
              memory: '4Gi',
            },
          },
        },
      },
      labelMemcached: {
        values: {
          replicaCount: 4,
          resources: {
            requests: {
              cpu: '1000m',
              memory: '4Gi',
            },
          },
        },
      },
      ingress: {
        ipName: 'thanos-query-frontend-ops',
        domains: ['thanos-query.ops.gitlab.net'],
      },
    },
  },

  'ops-stg': self.ops {
    name: 'ops-stg',
    query+: {
      config+: {
        replicas: 2,
        resources: {
          requests: {
            cpu: '1000m',
            memory: '4Gi',
          },
          limits: {
            memory: '4Gi',
          },
        },
        tracing: stackdriverTracing,
      },
    },
    queryFrontend+: {
      config+: {
        replicas: 2,
        resources: {
          requests: {
            cpu: '1000m',
            memory: '2Gi',
          },
          limits: {
            memory: '2Gi',
          },
        },
        tracing: stackdriverTracing,
      },
      queryRangeMemcached: {
        values: {
          replicaCount: 2,
          resources: {
            requests: {
              memory: '1Gi',
            },
          },
        },
      },
      labelMemcached: {
        values: {
          replicaCount: 2,
          resources: {
            requests: {
              memory: '1Gi',
            },
          },
        },
      },
      ingress: {
        ipName: 'thanos-query-stg-frontend-ops',
        domains: ['thanos-query-stg.ops.gitlab.net'],
      },
    },
  },

  pre: defaultConfig {
    name: 'pre',
    store+: {
      bucketName: 'gitlab-pre-prometheus',
      config+: {
        resources: defaultStoreShardResources,
        shards: 1,
        replicas: 1,
        tracing: stackdriverTracing,
      },
      diskSize: defaultStoreShardDiskSize,
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(),
  },

  'pre-2': defaultConfig {
    name: 'pre-2',
  },

  prdsub: defaultConfig {
    name: 'prdsub',
    store+: {
      bucketName: 'gitlab-prdsub-prometheus',
      config+: {
        resources: {
          requests: {
            cpu: '1',
            memory: '3Gi',
          },
          limits: {
            memory: '5Gi',
          },
        },
        shards: 1,
        replicas: 2,
        tracing: stackdriverTracing,
      },
      diskSize: '20Gi',
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(),
  },

  stgsub: defaultConfig {
    name: 'stgsub',
    store+: {
      bucketName: 'gitlab-stgsub-prometheus',
      config+: {
        resources: {
          requests: {
            cpu: '1',
            memory: '3Gi',
          },
          limits: {
            memory: '5Gi',
          },
        },
        shards: 1,
        replicas: 2,
        tracing: stackdriverTracing,
      },
      diskSize: '20Gi',
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(),
  },

  'stgsub-ref': defaultConfig {
    name: 'stgsub-ref',
    store+: {
      bucketName: 'gitlab-stgsub-ref-prometheus',
      config+: {
        resources: {
          requests: {
            cpu: '1',
            memory: '3Gi',
          },
          limits: {
            memory: '5Gi',
          },
        },
        shards: 1,
        replicas: 2,
        tracing: stackdriverTracing,
      },
      diskSize: '20Gi',
      diskStorageClass: defaultStorageClass,
    } + storeMemcached(),
  },

  // Dev environment, refers to a local development cluster.
  dev: defaultConfig {
    store+: {
      bucketName: 'dummy',
      config+: {
        logFormat: 'logfmt',
        resources: {},
        replicas: 1,
        shards: 1,
      },
      diskSize: '1Gi',
      diskStorageClass: null,
      indexMemcached: {
        values: {
          extraEnvVars+: {
            MEMCACHED_EXTRA_FLAGS: '--max-item-size=1M',
          },
          resources: {
            requests: {
              memory: '64Mi',
            },
          },
          replicaCount: 1,
        },
      },
      bucketMemcached: {
        values: {
          extraEnvVars+: {
            MEMCACHED_EXTRA_FLAGS: '--max-item-size=1M',
          },
          resources: {
            requests: {
              memory: '64Mi',
            },
          },
          replicaCount: 1,
        },
      },
    },
    query+: {
      config+: {
        logFormat: 'logfmt',
        replicas: 1,
        resources: {},
      },
    },
    queryFrontend+: {
      config+: {
        logFormat: 'logfmt',
        replicas: 1,
        resources: {},
      },
      queryRangeMemcached: {
        values: {
          extraEnvVars+: {
            MEMCACHED_EXTRA_FLAGS: '--max-item-size=1M',
          },
          replicaCount: 1,
          resources: {
            requests: {
              memory: '64Mi',
            },
          },
        },
      },
      labelMemcached: {
        values: {
          extraEnvVars+: {
            MEMCACHED_EXTRA_FLAGS: '--max-item-size=1M',
          },
          replicaCount: 1,
          resources: {
            requests: {
              memory: '64Mi',
            },
          },
        },
      },
    },
  },
}

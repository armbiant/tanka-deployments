# Thanos

## Deployment

### Secrets

When deploying IAP-protected ingress (which is the case in ops but not dev), we
need to configure an OAuth secret for the IAP.

The secret `thanos-query-frontend-iap-oauth` is managed by
[`external-secrets`](https://external-secrets.io/) and stored in
[Vault](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/vault/vault.md)
under `k8s/ops-gitlab-net/monitoring/thanos/iap-oauth`.

The keys we need to populate are `client_id`, and `client_secret`. These must
correspond to existing GCP oauth clients that list URIs for this deployment's
configured ingress domain name. You will have had to inject a domain name into
`queryFrontendIngress()` in `thanos.libsonnet` already. These GCP oauth clients
are managed manually, and an example can be found
[here](https://console.cloud.google.com/apis/credentials/oauthclient/65580314219-jqstgo44nhpp9v525u5ktlqbbum791jk.apps.googleusercontent.com?project=gitlab-staging-1).
We usually use a single oauth client named "monitoring" in each environment,
that should already exist.

## Quirks

Does not create its namespace (`monitoring`), as it shares a namespace with the
[gitlab-monitoring helm release](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/30-gitlab-monitoring).
On fresh dev clusters, you'll either want to deploy that first, or manually
create the `monitoring` namespace.

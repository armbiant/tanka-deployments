local common = import 'gitlab/common.libsonnet';
local secrets = import 'secrets.jsonnet';
local query = import 'thanos/query.libsonnet';
local store = import 'thanos/store.libsonnet';
local values = import 'values.jsonnet';

{
  local appName = 'thanos',
  local namespaceName = 'monitoring',
  local namespaceNameStg = 'monitoring-stg',

  envsData:: {
    gprd: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      store: store.newStore(
        values.gprd,
        namespaceName
      ),
    },
    gstg: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      store: store.newStore(
        values.gstg,
        namespaceName
      ),
    },
    ops: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      externalSecretsRole:: 'thanos',
      store: store.newStore(
        values.ops,
        namespaceName
      ),
      query: query.newQuery(
        values.ops,
        namespaceName
      ) + query.addIngress(
        values.ops.queryFrontend.ingress.ipName,
        values.ops.queryFrontend.ingress.domains,
      ),
      query_secrets: secrets.query_frontend.new('ops', namespaceName, appName),
    },
    'ops-stg': {
      namespaceName:: namespaceNameStg,
      createNamespace:: true,
      externalSecretsRole:: 'thanos',
      query: query.newQuery(
        values['ops-stg'],
        namespaceNameStg
      ) + query.addIngress(
        values['ops-stg'].queryFrontend.ingress.ipName,
        values['ops-stg'].queryFrontend.ingress.domains,
      ),
      query_secrets: secrets.query_frontend.new('ops-stg', namespaceName, appName),
    },
    pre: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      store: store.newStore(
        values.pre,
        namespaceName
      ),
    },
    'pre-2': {
      namespaceName:: namespaceName,
      createNamespace:: false,
    },
    prdsub: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      store: store.newStore(
        values.prdsub,
        namespaceName
      ),
    },
    stgsub: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      store: store.newStore(
        values.stgsub,
        namespaceName
      ),
    },
    'stgsub-ref': {
      namespaceName:: namespaceName,
      createNamespace:: true,
      store: store.newStore(
        values['stgsub-ref'],
        namespaceName
      ),
    },
    dev: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      store: store.newStore(
        values.dev,
        namespaceName
      ),
      query: query.newQuery(
        values.dev,
        namespaceName
      ),
    },
  },

  envs: common.genGitlabEnvs(appName, $.envsData),
}

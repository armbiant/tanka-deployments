local clusters = import 'gitlab/clusters.libsonnet';
local externalSecrets = import 'gitlab/external-secrets.libsonnet';

{
  query_frontend:: {
    local labels = {
      'app.kubernetes.io/name': 'thanos-query-frontend',
      'app.kubernetes.io/instance': 'thanos-query-frontend',
    },

    new(env, namespace, appName):: {
      local secretKey(name) = externalSecrets.k8sAppSecretKey(clusters[env].clusterName, namespace, appName, name),

      iap_oauth:
        externalSecrets.externalSecret(
          'thanos-query-frontend-iap-oauth',
          appName,
          namespace,
          clusters[env].clusterName,
          labels=labels,
          data={
            client_id: {
              key: secretKey('iap-oauth'),
              property: 'client_id',
              version: 1,
            },
            client_secret: {
              key: secretKey('iap-oauth'),
              property: 'client_secret',
              version: 1,
            },
          },
        ),
    },
  },
}

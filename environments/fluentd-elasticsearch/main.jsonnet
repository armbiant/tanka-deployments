local fluentdElasticsearch = import 'fluentd/fluentd-elasticsearch.libsonnet';
local common = import 'gitlab/common.libsonnet';
local secrets = import 'secrets.jsonnet';
local values = import 'values.jsonnet';

{
  local appName = 'fluentd-elasticsearch',
  local namespaceName = 'logging',
  local labels = values.labels(appName).selector,

  envsData:: {
    pre: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      externalSecretsRole:: 'fluentd',
      secretStorePaths:: ['shared'],
      fluentdElasticsearch: fluentdElasticsearch.new(name=appName, envValues=values.envs(appName).pre),
      fluentdSecrets: secrets.new('pre', namespaceName, appName, labels),
    },
    ops: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      externalSecretsRole:: 'fluentd',
      secretStorePaths:: ['shared'],
      fluentdElasticsearch: fluentdElasticsearch.new(name=appName, envValues=values.envs(appName).ops),
      fluentdSecrets: secrets.new('ops', namespaceName, appName, labels),
    },
    gstg: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      externalSecretsRole:: 'fluentd',
      secretStorePaths:: ['shared'],
      fluentdElasticsearch: fluentdElasticsearch.new(name=appName, envValues=values.envs(appName).gstg),
      fluentdSecrets: secrets.new('gstg', namespaceName, appName, labels),
    },
    'gstg-us-east1-b': $.envsData.gstg {
      fluentdSecrets: secrets.new('gstg-us-east1-b', namespaceName, appName, labels),
    },
    'gstg-us-east1-c': $.envsData.gstg {
      fluentdSecrets: secrets.new('gstg-us-east1-c', namespaceName, appName, labels),
    },
    'gstg-us-east1-d': $.envsData.gstg {
      fluentdSecrets: secrets.new('gstg-us-east1-d', namespaceName, appName, labels),
    },
    gprd: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      externalSecretsRole:: 'fluentd',
      secretStorePaths:: ['shared'],
      fluentdElasticsearch: fluentdElasticsearch.new(name=appName, envValues=values.envs(appName).gprd),
      fluentdSecrets: secrets.new('gprd', namespaceName, appName, labels, production_logs=true),
    },
    'gprd-us-east1-b': $.envsData.gprd {
      fluentdSecrets: secrets.new('gprd-us-east1-b', namespaceName, appName, labels, production_logs=true),
    },
    'gprd-us-east1-c': $.envsData.gprd {
      fluentdSecrets: secrets.new('gprd-us-east1-c', namespaceName, appName, labels, production_logs=true),
    },
    'gprd-us-east1-d': $.envsData.gprd {
      fluentdSecrets: secrets.new('gprd-us-east1-d', namespaceName, appName, labels, production_logs=true),
    },
  },

  envs: common.genGitlabEnvs(appName, $.envsData, resourceDefaults={ labels: labels }),
}

# fluentd-elasticsearch

This deployment configures a fluentd process to consume container logs into our ES stack.

## Adding a new logfile

To configure a new logfile, add a new section to [logging-config.yaml](../../lib/fluentd/logging-config.yaml) like the following:

```
- name: <name>
  pathMatch: "<glob>"
  topic_prefix: <topic> # For example, `pubsub-rails-inf`
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "<type>"
    stage: "<stage>" # main or cny
```

The glob pattern matches the log files under /var/log/containers/{glob}.log on the GKE node.
It's set from pathMatch if defined, otherwise it will be `*{name}*` by default.
It's best to set pathMatch to match exactly the container logs you want to pull.

You can preview the generated fluentd configuration by using `tk eval`:

```
tk eval environments/fluentd-elasticsearch | jq -r '.envs."fluentd-elasticsearch/gprd".data.fluentdElasticsearch.configMaps.config.data."index.containers.input.conf"'
```

local clusters = import 'gitlab/clusters.libsonnet';

{
  labels(appName):: {
    selector: {
      'app.kubernetes.io/instance': appName,
      'app.kubernetes.io/name': appName,
    },
    resources: {
      stage: 'main',
    },
  },

  envs(appName):: {
    pre: {
      envName: 'pre',
      imagePullSecrets: ['%s-image-pull-secret' % appName],
      googleProject: clusters.pre.googleProject,
      labels: $.labels(appName),
    },
    ops: {
      envName: 'ops',
      imagePullSecrets: ['%s-image-pull-secret' % appName],
      googleProject: clusters.ops.googleProject,
      labels: $.labels(appName),
    },
    gstg: {
      envName: 'gstg',
      imagePullSecrets: ['%s-image-pull-secret' % appName],
      googleProject: clusters.gstg.googleProject,
      labels: $.labels(appName),
      resources: {
        limits: {
          cpu: '1',
          memory: '2G',
        },
        requests: {
          cpu: '100m',
          memory: '200Mi',
        },
      },
    },
    gprd: {
      envName: 'gprd',
      imagePullSecrets: ['%s-image-pull-secret' % appName],
      googleProject: clusters.gprd.googleProject,
      labels: $.labels(appName),
      resources: {
        limits: {
          cpu: '1',
          memory: '2G',
        },
        requests: {
          cpu: '100m',
          memory: '200Mi',
        },
      },
    },
  },
}

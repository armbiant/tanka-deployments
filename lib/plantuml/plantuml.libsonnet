local common = import 'gitlab/common.libsonnet';
local k = import 'k.libsonnet';
local configMap = k.core.v1.configMap,
      hpa = k.autoscaling.v2.horizontalPodAutoscaler,
      hpaMetricSpec = k.autoscaling.v2.metricSpec,
      networkPolicy = k.networking.v1.networkPolicy,
      networkPolicyEgressRule = k.networking.v1.networkPolicyEgressRule,
      networkPolicyPort = k.networking.v1.networkPolicyPort,
      ipBlock = k.networking.v1.ipBlock,
      podDisruptionBudget = k.policy.v1.podDisruptionBudget,
      service = k.core.v1.service,
      servicePort = k.core.v1.servicePort,
      ingress = k.networking.v1.ingress,
      deployment = k.apps.v1.deployment,
      container = k.core.v1.container,
      containerPort = k.core.v1.containerPort,
      volumeMount = k.core.v1.volumeMount,
      volume = k.core.v1.volume;

{
  local defaults = {
    domains: ['gitlab.example.com'],
    nodeSelector: {},
    plantumlImage: 'plantuml/plantuml-server:jetty-v1.2021.7',
    redirect_url: 'https://gitlab.example.com',
    replicas: 2,
    resources: {
      requests: { cpu: '500m', memory: '500M' },
      limits: { cpu: 1, memory: '2G' },
    },
    server_name: 'example.plantuml.gitlab-static.net',
  },
  new(
    name='plantuml',
    envValues={}
  )::

    local values = defaults + envValues;
    local selector = {
      'app.kubernetes.io/instance': name,
      'app.kubernetes.io/name': name,
    };

    {
      networkPolicy: networkPolicy.new(name)
                     + networkPolicy.spec.withEgress(
                       [
                         networkPolicyEgressRule.withPorts([{ port: 53, protocol: 'UDP' }])
                         + networkPolicyEgressRule.withTo([{ ipBlock: ipBlock.withCidr('10.0.0.0/8') }]),
                         networkPolicyEgressRule.withTo(
                           [
                             { ipBlock:
                               ipBlock.withCidr('0.0.0.0/0')
                               + ipBlock.withExcept(['10.0.0.0/8', '169.254.169.254/32']) },
                           ]
                         ),
                       ]
                     )
                     + networkPolicy.spec.podSelector.withMatchLabels(selector)
                     + networkPolicy.spec.withPolicyTypes(['Egress']),
      podDisruptionBudget: podDisruptionBudget.new(name)
                           + podDisruptionBudget.spec.withMaxUnavailable('5%')
                           + podDisruptionBudget.spec.selector.withMatchLabels(selector),
      configMap: configMap.new(
        name=name,
        data={
          'nginx.conf':
            |||
              limit_req_zone $http_x_forwarded_for zone=rlimit:10m rate=2r/s;
              log_format json_combined escape=json
              '{'
                '"remote_addr": "$remote_addr",'
                '"remote_user": "$remote_user",'
                '"timestamp": "$time_iso8601",'
                '"request": "$request",'
                '"status": "$status",'
                '"request_time": "$request_time",'
                '"body_bytes_sent": "$body_bytes_sent",'
                '"http_referer": "$http_referer",'
                '"http_user_agent": "$http_user_agent",'
                '"http_x_forwarded_for": "$http_x_forwarded_for",'
                '"upstream_response_time": "$upstream_response_time",'
                '"connection":"$connection",'
                '"connection_requests": "$connection_requests",'
                '"connections_active": "$connections_active",'
                '"connections_reading": "$connections_reading",'
                '"connections_writing": "$connections_writing",'
                '"connections_waiting": "$connections_waiting"'
              '}';
              server {
                listen 80 default_server;
                listen [::]:80 default_server;

                server_name %(server_name)s;
                access_log /var/log/nginx/access.log json_combined;
                server_tokens off;
                location / {
                    return 301 %(redirect_url)s;
                }
                location ~ ^/(png|svg) {
                  limit_req zone=rlimit burst=5 nodelay;
                  limit_req_status 429;
                  proxy_set_header X-Forwarded-Host $host;
                  proxy_set_header X-Real-IP $remote_addr;
                  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                  if ($http_x_forwarded_proto = 'http') {
                       return 301 https://$server_name$request_uri;
                  }
                  limit_except GET {
                      deny all;
                  }
                  proxy_pass http://localhost:8080;
                  proxy_hide_header access-control-allow-origin;
                  proxy_hide_header x-powered-by;
                  add_header X-Frame-Options SAMEORIGIN always;
                }
              }
            ||| % values,
        },
      ),
      service: service.new(
                 name=name,
                 selector=selector,
                 ports=[
                   servicePort.newNamed(name, 80, 80) + servicePort.withProtocol('TCP'),
                 ],
               )
               + service.metadata.withAnnotations({ 'beta.cloud.google.com/backend-config': '{"ports": {"80":"%s"}}' % name },)
               + service.spec.withType('NodePort'),
      deployment: deployment.new(
                    name=name,
                    replicas=values.replicas,
                    containers=[
                      container.new(name, values.plantumlImage)
                      + container.withArgs([
                        '-Djetty.port=8080',
                        '--module=http-forwarded',
                      ],)
                      + container.withPorts(
                        [
                          containerPort.newNamed(8080, 'web')
                          + containerPort.withProtocol('TCP'),
                        ]
                      )
                      + container.resources.withRequests(values.resources.requests)
                      + container.resources.withLimits(values.resources.limits)
                      + container.withImagePullPolicy('IfNotPresent'),
                      container.new('nginx', 'nginx:1.19')
                      + container.withPorts([containerPort.newNamed(80, 'nginx')])
                      + container.resources.withRequests({ cpu: '100m', memory: '50Mi' })
                      + container.livenessProbe.httpGet.withPort(80)
                      + container.livenessProbe.httpGet.withPath('/png')
                      + container.readinessProbe.httpGet.withPort(80)
                      + container.readinessProbe.httpGet.withPath('/png')
                      + container.withImagePullPolicy('IfNotPresent')
                      + container.withVolumeMounts([
                        volumeMount.new('conf', '/etc/nginx/conf.d'),
                      ]),
                    ],
                    podLabels=values.selector,
                  )
                  + deployment.spec.template.metadata.withLabels(
                    selector {
                      stage: 'main',
                      tier: 'sv',
                      type: name,
                    },
                  )
                  + deployment.spec.selector.withMatchLabels(selector)
                  + (if values.nodeSelector != {} then deployment.spec.template.spec.withNodeSelector(values.nodeSelector) else {})
                  + deployment.spec.template.spec.withVolumes([
                    volume.fromConfigMap('conf', name),
                  ]),
      hpa: hpa.new(name)
           + hpa.spec.withMinReplicas(values.replicas)
           + hpa.spec.withMaxReplicas(10)
           + hpa.spec.withMetrics([
             hpaMetricSpec.withType('Resource')
             + hpaMetricSpec.resource.withName('cpu')
             + hpaMetricSpec.resource.target.withType('Utilization')
             + hpaMetricSpec.resource.target.withAverageUtilization(75),
           ])
           + hpa.spec.scaleTargetRef.withApiVersion('apps/v1')
           + hpa.spec.scaleTargetRef.withKind('Deployment')
           + hpa.spec.scaleTargetRef.withName(name),
      ingress: ingress.new(name)
               + ingress.metadata.withAnnotations(
                 {
                   'kubernetes.io/ingress.allow-http': 'false',
                   'kubernetes.io/ingress.global-static-ip-name': 'plantuml-gke-%(name)s' % values,
                   'networking.gke.io/managed-certificates': 'plantuml',
                 },
               )
               + ingress.spec.defaultBackend.service.withName(name)
               + ingress.spec.defaultBackend.service.port.withNumber(80)
               + ingress.spec.withRules([
                 {
                   http: {
                     paths: [
                       {
                         backend: {
                           service: {
                             name: name,
                             port: {
                               number: 80,
                             },
                           },
                         },
                         path: '/*',
                         pathType: 'ImplementationSpecific',
                       },
                     ],
                   },
                 },
               ],),
      backendConfig: {
        apiVersion: 'cloud.google.com/v1',
        kind: 'BackendConfig',
        metadata: {
          name: name,
        },
        spec: {
          cdn: {
            cachePolicy: {
              includeHost: true,
              includeProtocol: true,
              includeQueryString: false,
            },
            enabled: true,
          },
        },
      },
      managedCertificate: common.managedCert(
        name=name,
        domains=values.domains,
      ),
    },
}

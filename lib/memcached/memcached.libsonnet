local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);
local common = import 'gitlab/common.libsonnet';

local serverMemoryOverheadMbytes = 50;

{
  new(name, namespace, type, nodePool=null, chartValues={}, metricRelabelings=[])::
    local memBytes = common.toBytes(chartValues.resources.requests.memory);
    local memMBytes = memBytes / 1024 / 1024;
    assert memMBytes > serverMemoryOverheadMbytes : |||
      %dMB is not enough memory to run memcached - you'll need at least %dMB
      plus the space you want for the cache.
    ||| % [memMBytes, serverMemoryOverheadMbytes];
    local cacheSizeMB = memMBytes - serverMemoryOverheadMbytes;

    // This provisions a Secret containing the memcached password, which is not
    // really a Secret because we use empty string. We are not using auth for
    // memcached, and are relying the fact that we don't expose it to the
    // internet to secure it.
    // If we want to use a proper password, we should patch out the Secret to
    // null, and document the structure of the secret to be manually configured
    // pre-deployment.
    local defaultChartValues = {
      nameOverride: name,

      architecture: 'high-availability',

      persistence: {
        enabled: false,
      },

      metrics: {
        enabled: true,
        serviceMonitor: {
          enabled: true,
          labels: {
            'gitlab.com/prometheus-instance': 'prometheus-system',
          },
          relabelings: metricRelabelings,
        },
      },

      podLabels: {
        type: type,
        deployment: 'memcached-%s' % name,
      },

      livenessProbe: {
        enabled: true,
        initialDelaySeconds: 30,
      },

      readinessProbe: {
        enabled: true,
        initialDelaySeconds: 10,
      },

      startupProbe: {
        enabled: true,
        initialDelaySeconds: 30,
      },

      [if nodePool != null then 'nodeAffinityPreset']: {
        type: 'hard',
        key: 'type',
        values: [nodePool],
      },

      pdb: {
        create: true,
      },

      // extraEnv is an array, but we will work with objects to enable patching
      // semantics, with fallback to default values, before converting back to
      // an array.
      extraEnvVars: {
        MEMCACHED_EXTRA_FLAGS: '--max-item-size=16M',
        MEMCACHED_CACHE_SIZE: '%d' % cacheSizeMB,
        MEMCACHED_MAX_CONNECTIONS: '4096',
      },
    };

    local mergedChartValues = defaultChartValues + chartValues;
    local mergedChartValuesWithEnv = mergedChartValues {
      extraEnvVars: [{ name: key, value: mergedChartValues.extraEnvVars[key] } for key in std.objectFields(mergedChartValues.extraEnvVars)],
    };

    helm.template(
      'memcached',
      '../../charts/memcached',
      { values: mergedChartValuesWithEnv, namespace: namespace },
    ),
}

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local common = import 'gitlab/common.libsonnet';
local helm = tanka.helm.new(std.thisFile);

{
  new(cluster, namespace, envValues, externalMaster, k8sCluster, nodeSelector, resources, replicaCount, specialConfiguration='save 900 1', tolerations, zoneAntiAffinity, storageClass='pd-balanced', dataStorageSize='20Gi')::
    local disableCommands = [
      'FLUSHDB',
      'FLUSHALL',
      'KEYS',
    ];

    local commonConfiguration = |||
      appendonly no
      client-output-buffer-limit normal 0 0 0
      client-output-buffer-limit pubsub 32mb 8mb 60
      io-threads 2
      maxclients 40000
      tcp-keepalive 60
      timeout 1200
      replica-priority 100
    |||;

    local resourcesWithDefaults = {
      // cpu:    1 main thread + 1 I/O thread + 1 bgsave + 1 for good measure
      // memory: redis-ratelimiting has 16 GiB available
      redis: {
        memory: '16Gi',
        cpu: 4,
      },
      sentinel: {
        memory: '2Gi',
        cpu: 1,
      },
      exporter: {
        memory: '1Gi',
        cpu: 0.5,
      },
      metrics: {
        memory: '1Gi',
        cpu: 0.5,
      },
    } + resources;

    local resourcesLimitsAndRequests = std.foldl(
      function(memo, containerName)
        memo {
          [containerName]: {
            requests: resourcesWithDefaults[containerName],
            limits: resourcesWithDefaults[containerName],
          },
        },
      std.objectFields(resourcesWithDefaults),
      {}
    );


    local configMapName = '%s-process-exporter' % [cluster];
    local podMonitorName = '%s-process-exporter' % [cluster];

    local externalDNSDomain = '%s.gke.gitlab.net' % k8sCluster;
    local commonLabels = if std.objectHas(envValues, 'commonLabels') then
      envValues.commonLabels
    else {};

    // https://github.com/bitnami/charts/blob/master/bitnami/redis/values.yaml
    local values = {
      global: {
        storageClass: storageClass,
      },
      architecture: 'replication',
      auth: {
        sentinel: false,
      },
      commonConfiguration: commonConfiguration + specialConfiguration,
      image: {
        // renovate: datasource=docker depName=bitnami/redis versioning=docker
        tag: '6.2.7-debian-10-r24',
      },
      metrics: {
        enabled: true,
        resources: resourcesLimitsAndRequests.metrics,
        serviceMonitor: {
          podTargetLabels: std.objectFields(commonLabels),
          enabled: true,
          additionalLabels: {
            'gitlab.com/prometheus-instance': 'prometheus-system',
          },
        },
      },
      replica: {
        replicaCount: replicaCount,
        disableCommands: disableCommands,
        externalMaster: externalMaster,
        nodeSelector: nodeSelector,
        tolerations: tolerations,
        resources: resourcesLimitsAndRequests.redis,
        shareProcessNamespace: true,
        podLabels: commonLabels,
        sidecars: [
          {
            name: 'process-exporter',
            image: 'ncabatoff/process-exporter:0.7.10',
            resources: resourcesLimitsAndRequests.exporter,
            args: ['-config.path', '/etc/process-exporter/process-exporter.yml', '-recheck', '-gather-smaps=false'],
            ports: [{ name: 'process', containerPort: 9256 }],
            securityContext: {
              runAsUser: 1001,
              runAsGroup: 1001,
            },
            volumeMounts: [
              { name: 'process-exporter-config', mountPath: '/etc/process-exporter' },
            ],
          },
        ],
        extraVolumes: [
          { name: 'process-exporter-config', configMap: { name: configMapName } },
        ],
        persistence: {
          enabled: true,
          size: dataStorageSize,
        },
      },
      pdb: {
        create: true,
        minAvailable: '',
        maxUnavailable: '5%',
      },
      sentinel: {
        enabled: true,
        downAfterMilliseconds: 10000,
        externalMaster: externalMaster,
        image: {
          // renovate: datasource=docker depName=bitnami/redis-sentinel versioning=docker
          tag: '6.2.7-debian-10-r24',
        },
        resources: resourcesLimitsAndRequests.sentinel,
      },
      useExternalDNS: {
        enabled: true,
        suffix: externalDNSDomain,
        annotationKey: false,
      },
    } + (
      if zoneAntiAffinity then
        {
          replica+: {
            affinity: {
              podAntiAffinity: {
                requiredDuringSchedulingIgnoredDuringExecution: [
                  {
                    labelSelector: {
                      matchLabels: {
                        'app.kubernetes.io/component': 'node',
                        'app.kubernetes.io/instance': cluster,
                        'app.kubernetes.io/name': 'redis',
                      },
                    },
                    namespaces: [namespace],
                    topologyKey: 'topology.kubernetes.io/zone',
                  },
                ],
              },
            },
          },
        }
      else
        {}
    );

    {
      redis: helm.template(
        cluster,
        '../../charts/redis-' + k8sCluster,
        { namespace: namespace, values: std.mergePatch(values, envValues) }
      ),
      configMap: {
        apiVersion: 'v1',
        kind: 'ConfigMap',
        metadata: {
          name: configMapName,
          labels: commonLabels,
        },
        data: {
          // please note that we have no config reloader for this config
          // so any changes here will require a forceful restart (e.g. pod delete)
          // to take effect.
          'process-exporter.yml': |||
            ---
            process_names:
            - name: redis-server
              comm:
              - redis-server
              cmdline:
              - 'redis-server \*:6379'
            - name: redis-sentinel
              comm:
              - redis-server
              cmdline:
              - 'redis-server \*:26379 \[sentinel\]'
          |||,
        },
      },
      podMonitor: {
        apiVersion: 'monitoring.coreos.com/v1',
        kind: 'PodMonitor',
        metadata: {
          name: podMonitorName,
          labels: commonLabels {
            'app.kubernetes.io/component': 'metrics',
            'app.kubernetes.io/instance': cluster,
            'app.kubernetes.io/name': 'redis',
            'gitlab.com/prometheus-instance': 'prometheus-system',
          },
        },
        spec: {
          podTargetLabels: std.objectFields(commonLabels),
          selector: {
            matchLabels: {
              'app.kubernetes.io/component': 'node',
              'app.kubernetes.io/instance': cluster,
              'app.kubernetes.io/name': 'redis',
            },
          },
          podMetricsEndpoints: [{ port: 'process' }],
        },
      },
      loadBalancers: [
        {
          apiVersion: 'v1',
          kind: 'Service',
          metadata: {
            annotations: {
              'external-dns.alpha.kubernetes.io/hostname': '%s-node-%s.%s.%s' % [cluster, i, cluster, externalDNSDomain],
              'networking.gke.io/load-balancer-type': 'Internal',
              'networking.gke.io/internal-load-balancer-allow-global-access': 'true',
            },
            labels: commonLabels {
              'app.kubernetes.io/component': 'node',
              'app.kubernetes.io/instance': cluster,
              'app.kubernetes.io/name': 'redis',
            },
            name: '%s-node-%s' % [cluster, i],
            namespace: namespace,
          },
          spec: {
            externalTrafficPolicy: 'Local',
            ports: [
              {
                name: 'sentinel',
                port: 26379,
                protocol: 'TCP',
                targetPort: 26379,
              },
              {
                name: 'redis',
                port: 6379,
                protocol: 'TCP',
                targetPort: 6379,
              },
            ],
            selector: {
              'statefulset.kubernetes.io/pod-name': '%s-node-%s' % [cluster, i],
            },
            type: 'LoadBalancer',
          },
        }
        for i in std.range(0, replicaCount - 1)
      ],
    },
}

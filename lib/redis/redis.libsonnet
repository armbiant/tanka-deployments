local instance = import 'redis/instance.libsonnet';

{
  new(k8sCluster, appName, cluster, nodeSelector=null, tolerations=[], zoneAntiAffinity=false)::
    {
      instance: instance.new(
        cluster.name,
        appName,
        envValues={
          commonLabels: {
            stage: 'main',
            tier: 'db',
            type: cluster.name,
            deployment: cluster.name,
          },
          auth: {
            existingSecret: '%s-password' % cluster.name,
            existingSecretPasswordKey: 'redis-password',
            usePasswordFiles: true,
          },
        },
        externalMaster=cluster.externalMaster,
        k8sCluster=k8sCluster,
        nodeSelector=nodeSelector,
        resources=if std.objectHas(cluster, 'resources') then cluster.resources else {},
        replicaCount=cluster.replicaCount,
        specialConfiguration=cluster.specialConfiguration,
        tolerations=tolerations,
        zoneAntiAffinity=zoneAntiAffinity,
        storageClass=cluster.storageClass,
        dataStorageSize=cluster.dataStorageSize,
      ),
    },
}

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);
{
  defaultESCluster(name='elasticsearch', chartPath='../../../charts/elasticsearch', conf={}):: {
    elasticsearch: helm.template(name, chartPath, conf),
  },
}

local clusters = import 'gitlab/clusters.libsonnet';
local common = import 'gitlab/common.libsonnet';
local image = import 'image.libsonnet';
local k = import 'k.libsonnet';
local thanos = import 'kube-thanos/thanos.libsonnet';
local memcached = import 'memcached/memcached.libsonnet';
local thanos_common = import 'thanos-common.libsonnet';

local
  podDisruptionBudget = k.policy.v1.podDisruptionBudget,
  service = k.core.v1.service;

{
  newQuery(envValues, namespace)::

    local defaults = {
      query: {
        config: {
          version: image.tag,
          image: std.format('%s:%s', [image.repository, image.tag]),
          logFormat: 'json',
          serviceMonitor: true,
          replicas: 1,
          objectStorageConfig: {
            name: 'thanos-objstore',
            key: 'objstore-gcs.yaml',
          },
          namespace: namespace,
          replicaLabels: ['replica', 'prometheus_replica'],
        },
      },
      queryFrontend: {
        config: {
          version: image.tag,
          image: std.format('%s:%s', [image.repository, image.tag]),
          logFormat: 'json',
          serviceMonitor: true,
          replicas: 1,
          objectStorageConfig: {
            name: 'thanos-objstore',
            key: 'objstore-gcs.yaml',
          },
          namespace: namespace,
        },
      },
    };

    local values = defaults + envValues;

    local endpointFlagPatch = {
      spec+: {
        template+: {
          spec+: {
            containers: [
              local c = super.containers[0];
              c {
                // --store is deprecated in favor of --endpoint since v0.24.0
                // https://github.com/thanos-io/thanos/releases/tag/v0.24.0
                // https://github.com/thanos-io/thanos/pull/4282
                args: std.map(function(arg) std.native('regexSubst')('^--store=', arg, '--endpoint='), c.args),
              },
            ],
          },
        },
      },
    };

    local query = thanos.query(values.query.config) + {
      deployment+:
        common.withDeploymentPodLabel(values.type)
        + endpointFlagPatch,
      serviceMonitor+: thanos_common.withStandardMetricLabels() + thanos_common.withStandardPrometheusMonitor(),
    };

    local queryFrontend = values.queryFrontend {
      config+: {
        downstreamURL: 'http://%s.%s.svc.cluster.local.:%d' % [
          query.service.metadata.name,
          query.service.metadata.namespace,
          query.service.spec.ports[1].port,
        ],
      } + (
        if !std.objectHas(queryFrontend, 'labelMemcached') then
          {}
        else {
          labelsCache+: {
            type: 'memcached',
            config+: {
              addresses: ['memcached-thanos-qfe-labels.%s.svc.cluster.local:11211' % namespace],
              max_item_size: '16MB',
            },
          },
        }
      ) + (
        if !std.objectHas(queryFrontend, 'queryRangeMemcached') then
          {}
        else {
          queryRangeCache+: {
            type: 'memcached',
            config+: {
              addresses: ['memcached-thanos-qfe-query-range.%s.svc.cluster.local:11211' % namespace],
              max_item_size: '16MB',
            },
          },
        }
      ),
    };

    {
      query: query {
        // ILB for intra-VPC access, e.g. from thanos-rule. Once we've migrated all
        // components to kubernetes, we can use the service clusterIP instead
        // and remove this.
        // thanos-rule needs to retrieve non-stale data from thanos-query,
        // uncached, so cannot use the frontend.
        serviceInternal: self.service {
          metadata+: {
            name+: '-ilb',
            labels+: {
              'app.kubernetes.io/instance': super['app.kubernetes.io/instance'] + '-ilb',
            },
            annotations+: {
              'networking.gke.io/load-balancer-type': 'Internal',
              'external-dns.alpha.kubernetes.io/hostname':
                common.externalURL('thanos-query-internal', values.name, fqdn=true),
            },
          },
          spec+: {
            type: 'LoadBalancer',
          },
        },
        serviceAccount+: {
          metadata+: {
            annotations+: {
              'iam.gke.io/gcp-service-account':
                'thanos-query-sa@%s.iam.gserviceaccount.com' % [clusters[envValues.name].googleProject],
            },
          },
        },
        podDisruptionBudget:
          podDisruptionBudget.new(super.deployment.metadata.name)
          + podDisruptionBudget.metadata.withLabels(super.deployment.metadata.labels)
          + podDisruptionBudget.spec.selector.withMatchLabels(super.deployment.spec.selector.matchLabels)
          + podDisruptionBudget.spec.withMaxUnavailable('5%'),
      },

      queryFrontend: thanos.queryFrontend(queryFrontend.config) + {
        deployment+: common.withDeploymentPodLabel(values.type),
        serviceMonitor+: thanos_common.withStandardMetricLabels() + thanos_common.withStandardPrometheusMonitor(),

        // In order to use "classic" GKE HTTPS load balancing (not
        // "container-native", which we don't use anywhere yet at the time of
        // writing), we need a service with type=NodePort.
        service+: service.mixin.spec.withType('NodePort'),

        // ILB for intra-VPC access, e.g. from grafana. Once we've migrated all
        // components to kubernetes, we can use the service clusterIP instead
        // and remove this.
        serviceInternal: self.service {
          metadata+: {
            name+: '-ilb',
            labels+: {
              'app.kubernetes.io/instance': super['app.kubernetes.io/instance'] + '-ilb',
            },
            annotations+: {
              'networking.gke.io/load-balancer-type': 'Internal',
              'external-dns.alpha.kubernetes.io/hostname':
                common.externalURL('thanos-query-frontend-internal', values.name, fqdn=true),
            },
          },
          spec+: {
            type: 'LoadBalancer',
          },
        },
        serviceAccount+: {
          metadata+: {
            annotations+: {
              'iam.gke.io/gcp-service-account':
                'thanos-query-frontend-sa@%s.iam.gserviceaccount.com' % [clusters[envValues.name].googleProject],
            },
          },
        },
        podDisruptionBudget:
          podDisruptionBudget.new(super.deployment.metadata.name)
          + podDisruptionBudget.metadata.withLabels(super.deployment.metadata.labels)
          + podDisruptionBudget.spec.selector.withMatchLabels(super.deployment.spec.selector.matchLabels)
          + podDisruptionBudget.spec.withMaxUnavailable('5%'),
      },
      [if std.objectHas(queryFrontend, 'labelMemcached') then 'labelMemcached']:
        memcached.new(
          name='thanos-qfe-labels',
          namespace=namespace,
          type=values.type,
          chartValues=queryFrontend.labelMemcached.values,
          metricRelabelings=thanos_common.standardMetricLabels(),
        ),
      [if std.objectHas(queryFrontend, 'queryRangeMemcached') then 'queryRangeMemcached']:
        memcached.new(
          name='thanos-qfe-query-range',
          namespace=namespace,
          type=values.type,
          chartValues=queryFrontend.queryRangeMemcached.values,
          metricRelabelings=thanos_common.standardMetricLabels(),
        ),
    },

  addIngress(ipName, domains)::
    {
      local qfe = self.queryFrontend,

      queryFrontend+: {
        service+: {
          metadata+: {
            annotations+: {
              'cloud.google.com/backend-config': std.manifestJsonEx({ default: 'thanos-query-frontend' }, '  '),
            },
          },
        },
      },

      ingress: {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'Ingress',
        metadata: {
          name: 'thanos-query-frontend',
          annotations: {
            'kubernetes.io/ingress.global-static-ip-name': ipName,
            'networking.gke.io/managed-certificates': 'thanos-query-frontend',
          },
        },
        spec: {
          defaultBackend: {
            service: {
              name: qfe.service.metadata.name,
              port: {
                name: 'http',
              },
            },
          },
        },
      },

      backendConfig: common.backendConfig('thanos-query-frontend', 'thanos-query-frontend-iap-oauth'),
      managedCert: common.managedCert('thanos-query-frontend', domains),
    },
}

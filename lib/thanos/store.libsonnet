local clusters = import 'gitlab/clusters.libsonnet';
local common = import 'gitlab/common.libsonnet';
local image = import 'image.libsonnet';
local k = import 'k.libsonnet';
local thanos = import 'kube-thanos/thanos.libsonnet';
local memcached = import 'memcached/memcached.libsonnet';
local thanos_common = import 'thanos-common.libsonnet';

local
  podDisruptionBudget = k.policy.v1.podDisruptionBudget,
  serviceAccount = k.core.v1.serviceAccount;

// storageClass=null uses the default
local withDisk(size, storageClass) = {
  volumeClaimTemplate+: {
    spec: {
      accessModes: ['ReadWriteOnce'],
      storageClassName: storageClass,
      resources: {
        requests: {
          storage: size,
        },
      },
    },
  },
};

// Don't panic! This Secret is not a secret. The thanos-operator expects to
// load object store config for the store from a Secret, which can indeed
// contain private keys. We don't make use of this, instead using GKE
// workload identity for keyless bucket access. We still need to create
// this "Secret" to store the bucket name though.
local objectStoreConfig(bucketName) = {
  apiVersion: 'v1',
  kind: 'Secret',
  metadata: {
    name: 'thanos-objstore',
  },
  stringData: {
    'objstore-gcs.yaml': std.manifestYamlDoc({
      type: 'GCS',
      config: {
        bucket: bucketName,
      },
    }),
  },
};

{
  newStore(envValues, namespace)::

    local defaults = {
      store: {
        config: {
          version: image.tag,
          image: std.format('%s:%s', [image.repository, image.tag]),
          logFormat: 'json',
          serviceMonitor: true,
          replicas: 1,
          objectStorageConfig: {
            name: 'thanos-objstore',
            key: 'objstore-gcs.yaml',
          },
          namespace: namespace,

          indexCache+: (
            if !std.objectHas(envValues.store, 'indexMemcached') then {
              type: 'IN-MEMORY',
              config+: {
                max_size: '1GB',
              },
            }
            else {
              type: 'memcached',
              config+: {
                addresses: ['memcached-thanos-index-cache.%s.svc.cluster.local:11211' % namespace],
                max_item_size: '16MB',
                max_async_buffer_size: 1000,
                max_async_concurrency: 100,
                max_get_multi_batch_size: 100,
                max_get_multi_concurrency: 150,
                max_idle_connections: 150,
              },
            }
          ),

        } + (
          if !std.objectHas(envValues.store, 'bucketMemcached') then
            {}
          else {
            bucketCache+: {
              type: 'memcached',
              config+: {
                addresses: ['memcached-thanos-bucket-cache.%s.svc.cluster.local:11211' % namespace],
                max_item_size: '16MB',
                max_async_buffer_size: 1000,
                max_async_concurrency: 100,
                max_get_multi_batch_size: 100,
                max_get_multi_concurrency: 150,
                max_idle_connections: 150,
              },
            },
          }
        ) + withDisk(envValues.store.diskSize, envValues.store.diskStorageClass),
      },
    };
    local storeValues = defaults + envValues;

    local bucketSecret = objectStoreConfig(envValues.store.bucketName);
    local prometheusServiceAccount = self.prometheusServiceAccount(envValues.name, clusters[envValues.name].googleProject);

    local store = thanos.storeShards(storeValues.store.config) + {
      shards+: {
        ['shard%s' % i]: super['shard' + i] {
          statefulSet+: {
            metadata+: {
              labels: thanos_common.fixShardLabels(super.labels),
            },
            spec+: {
              local selector = super.selector,
              selector+: {
                matchLabels: thanos_common.fixShardLabels(super.matchLabels),
              },
              template+: {
                metadata+: {
                  labels: thanos_common.fixShardLabels(super.labels),
                },
                spec+: {
                  serviceAccountName: 'prometheus',
                  containers: [
                    if std.objectHas(envValues.store.config, 'extraFlags') && container.name == 'thanos-store' then container {
                      args+: envValues.store.config.extraFlags,
                    } + (
                      if std.objectHas(envValues.store.config, 'extraEnv') then
                        {
                          env+: envValues.store.config.extraEnv,
                        }
                      else
                        {}

                    )
                    else container
                    for container in super.containers
                  ],
                } + (
                  if std.objectHas(envValues.store, 'nodePool') then
                    common.withNodePoolAffinity(envValues.store.nodePool)
                  else
                    {}
                ),
              },
              volumeClaimTemplates: [
                volumeClaimTemplate {
                  metadata+: {
                    labels: thanos_common.fixShardLabels(super.labels),
                  },
                }
                for volumeClaimTemplate in super.volumeClaimTemplates
              ],
            },
          } + common.withDeploymentPodLabel(envValues.type, 'thanos-store'),

          service+: {
            metadata+: {
              labels: thanos_common.fixShardLabels(super.labels),
              annotations+: {
                'external-dns.alpha.kubernetes.io/hostname':
                  common.externalURL('thanos-store-internal-%d' % i, envValues.name, fqdn=true),
              },
            },
            spec+: {
              selector: thanos_common.fixShardLabels(super.selector),
            },
          },

          podDisruptionBudget:
            podDisruptionBudget.new(super.statefulSet.metadata.name)
            + podDisruptionBudget.metadata.withLabels(
              thanos_common.fixShardLabels(super.statefulSet.metadata.labels)
            )
            + podDisruptionBudget.spec.selector.withMatchLabels(
              thanos_common.fixShardLabels(super.statefulSet.spec.selector.matchLabels)
            )
            + podDisruptionBudget.spec.withMaxUnavailable('5%'),
        }
        for i in std.range(0, storeValues.store.config.shards - 1)
      },

      serviceMonitor+: {
        spec+: {
          endpoints: [
            endpoint {
              relabelings: [
                relabeling {
                  sourceLabels: (
                    // See comment on fixShardLabels() near the top of this file
                    if super.targetLabel == 'shard' then
                      ['__meta_kubernetes_service_label_store_observatorium_io_shard']
                    else super.sourceLabels
                  ),
                }
                for relabeling in super.relabelings
              ],
            }
            for endpoint in super.endpoints
          ],
        },
      } + thanos_common.withStandardMetricLabels(shard=null) + thanos_common.withStandardPrometheusMonitor(),
      serviceAccount:: prometheusServiceAccount,
    };

    {
      store: store,
      objectStoreConfig: bucketSecret,
      serviceAccount: prometheusServiceAccount,
      [if std.objectHas(envValues.store, 'indexMemcached') then 'indexMemcached']:
        memcached.new(
          name='thanos-index-cache',
          namespace=namespace,
          type=envValues.type,
          nodePool=(if std.objectHas(envValues.store, 'nodePool') then envValues.store.nodePool else null),
          chartValues=envValues.store.indexMemcached.values,
          metricRelabelings=thanos_common.standardMetricLabels(),
        ),
      [if std.objectHas(envValues.store, 'bucketMemcached') then 'bucketMemcached']:
        memcached.new(
          name='thanos-bucket-cache',
          namespace=namespace,
          type=envValues.type,
          nodePool=(if std.objectHas(envValues.store, 'nodePool') then envValues.store.nodePool else null),
          chartValues=envValues.store.bucketMemcached.values,
          metricRelabelings=thanos_common.standardMetricLabels(),
        ),
    },

  prometheusServiceAccount(env, googleProject)::
    serviceAccount.new(name='prometheus')
    + serviceAccount.metadata.withAnnotations({
      'iam.gke.io/gcp-service-account': '%s-prometheus-sa@%s.iam.gserviceaccount.com' % [env, googleProject],
    }),
}

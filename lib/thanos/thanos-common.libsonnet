local common = import 'gitlab/common.libsonnet';

// To be mixed into a ServiceMonitor


{
  withStandardMetricLabels(shard='default'):: {
    spec+: {
      endpoints: [
        endpoint {
          relabelings+: $.standardMetricLabels(shard=shard),
        }
        for endpoint in super.endpoints
      ],
    },
  },

  standardMetricLabels(type='thanos', stage='main', shard='default'):: common.standardMetricLabels(type, stage, shard),

  // Revert changes made to store shard labels from a kube-thanos change. We can't
  // change StatefulSet spec fields volumeClaimTemplates or selector, so if we
  // want to remove this patch we must migrate to new StatefulSets.
  //
  // See https://github.com/thanos-io/kube-thanos/issues/214.
  fixShardLabels(labels)::
    {
      [if key == 'store.thanos.io/shard' then 'store.observatorium.io/shard' else key]: labels[key]
      for key in std.objectFields(labels)
    },

  withStandardPrometheusMonitor(instance='prometheus-system'):: {
    metadata+: {
      labels+: {
        'gitlab.com/prometheus-instance': instance,
      },
    },
  },
}

// Thanos image configuration
{
  repository: 'quay.io/thanos/thanos',
  // renovate: datasource=docker depName=quay.io/thanos/thanos versioning=docker
  tag: 'v0.30.2',
}

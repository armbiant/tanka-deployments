---
# Note that topic_prefix is not actually part of the value schema defined
# by the chart, which is oriented around elasticsearch. We place it here to
# provide a mapping of fluentd tag patterns to topic names, to build a list
# of pubsub outout directives.

### Webservice log forwarding ###################################
# The following names (*{name}*) need to glob log files
# for the webservice pod for the front-end services in K8s.
#
# The first service that is being migrated to K8s is git, these
# names will need to change as more services are introduced.
#
# Example log file for rails in the git https/websocket pod: gitlab-webservice-64994c9bff-v9p6x_gitlab_gitlab-workhorse-82aa92b80579dc4f25fede4c8bde5d4b3d622e692c3099199093147f74bf5be5.log
# Example log file for workhorse in the git https/websocket pod: gitlab-webservice-64994c9bff-n58x8_gitlab_webservice-c8a9715665c4daad75c56ebdac18f43280137a8d62ecd124def04dc35bbecf0c.log

### API service
- name: api-workhorse
  pathMatch: "*-api-*gitlab_gitlab-workhorse*"
  topic_prefix: pubsub-workhorse-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "api"
    stage: "main"

- name: api-cny-workhorse
  pathMatch: "*-api-*gitlab-cny_gitlab-workhorse*"
  topic_prefix: pubsub-workhorse-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "api"
    stage: "cny"

- name: api-rails
  pathMatch: "*-api-*gitlab_webservice*"
  topic_prefix: pubsub-rails-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "api"
    stage: "main"
  # Exclude unstructured logs since we already collect {application,production}_json
  remove_events:
    - key: "subcomponent"
      value: "^application$"
    - key: "subcomponent"
      value: "^production$"

- name: api-cny-rails
  pathMatch: "*-api-*gitlab-cny_webservice*"
  topic_prefix: pubsub-rails-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "api"
    stage: "cny"
  # Exclude unstructured logs since we already collect {application,production}_json
  remove_events:
    - key: "subcomponent"
      value: "^application$"
    - key: "subcomponent"
      value: "^production$"

## Diagnostics Uploader
#  This is a sidecar that runs on all webservice containers.  It will
#  upload dianostic information periodically up to a GCS bucket
- name: diagnostics-uploader-main
  pathMatch: "*-*gitlab_diagnostics-uploader*"
  topic_prefix: pubsub-rails-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    stage: "main"
    subcomponent: "diagnostics-uploader"

- name: diagnostics-uploader-cny
  pathMatch: "*-*gitlab-cny_diagnostics-uploader*"
  topic_prefix: pubsub-rails-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    stage: "cny"
    subcomponent: "diagnostics-uploader"

### Git service
- name: git-workhorse
  pathMatch: "*-git-*gitlab_gitlab-workhorse*"
  topic_prefix: pubsub-workhorse-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "git"
    stage: "main"

- name: git-cny-workhorse
  pathMatch: "*-git-*gitlab-cny_gitlab-workhorse*"
  topic_prefix: pubsub-workhorse-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "git"
    stage: "cny"

- name: git-rails
  pathMatch: "*-git-*gitlab_webservice*"
  topic_prefix: pubsub-rails-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "git"
    stage: "main"
  # Exclude unstructured logs since we already collect {application,production}_json
  remove_events:
    - key: "subcomponent"
      value: "^application$"
    - key: "subcomponent"
      value: "^production$"

- name: git-cny-rails
  pathMatch: "*-git-*gitlab-cny_webservice*"
  topic_prefix: pubsub-rails-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "git"
    stage: "cny"
  # Exclude unstructured logs since we already collect {application,production}_json
  remove_events:
    - key: "subcomponent"
      value: "^application$"
    - key: "subcomponent"
      value: "^production$"

## Git Shell Service
- name: git-shell
  pathMatch: "*-shell-*_gitlab_gitlab-shell*"
  topic_prefix: pubsub-shell-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "git"
    stage: "main"

- name: git-cny-shell
  pathMatch: "*-shell-*_gitlab-cny_gitlab-shell*"
  topic_prefix: pubsub-shell-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "git"
    stage: "cny"

### Websockets service
- name: websockets-workhorse
  pathMatch: "*-websockets-*gitlab_gitlab-workhorse*"
  topic_prefix: pubsub-workhorse-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "websockets"
    stage: "main"

- name: websockets-cny-workhorse
  pathMatch: "*-websockets-*gitlab-cny_gitlab-workhorse*"
  topic_prefix: pubsub-workhorse-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "websockets"
    stage: "cny"

- name: websockets-rails
  pathMatch: "*-websockets-*gitlab_webservice*"
  topic_prefix: pubsub-rails-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "websockets"
    stage: "main"

- name: websockets-cny-rails
  pathMatch: "*-websockets-*gitlab-cny_webservice*"
  topic_prefix: pubsub-rails-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "websockets"
    stage: "cny"

### Web service
- name: web-workhorse
  pathMatch: "*-web-*gitlab_gitlab-workhorse*"
  topic_prefix: pubsub-workhorse-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "web"
    stage: "main"

- name: web-cny-workhorse
  pathMatch: "*-web-*gitlab-cny_gitlab-workhorse*"
  topic_prefix: pubsub-workhorse-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "web"
    stage: "cny"

- name: web-rails
  pathMatch: "*-web-*gitlab_webservice*"
  topic_prefix: pubsub-rails-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "web"
    stage: "main"
  # Exclude unstructured logs since we already collect {application,production}_json
  remove_events:
    - key: "subcomponent"
      value: "^application$"
    - key: "subcomponent"
      value: "^production$"

- name: web-cny-rails
  pathMatch: "*-web-*gitlab-cny_webservice*"
  topic_prefix: pubsub-rails-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "web"
    stage: "cny"
  # Exclude unstructured logs since we already collect {application,production}_json
  remove_events:
    - key: "subcomponent"
      value: "^application$"
    - key: "subcomponent"
      value: "^production$"

#################################################################

- name: gitlab-sidekiq-database-throttled
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    # This requires great care not to break.
    #
    # Note the ruby expression `json.exception` or `exception`, referring
    # to itself. As we cut over to pubsub-enabled environments, we'll stop
    # prefixing logs with `json.`, causing this expression to break. The
    # stream configs in this file are shared across environments, to avoid
    # having to deal with (streams * environments) configs in helm,
    # because of the fiddliness around merging arrays.
    #
    # A hacky compromise is to temporarily break exception parsing in
    # nonprod environments, changing this just as we go to prod.
    #
    # See https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11655
    # for more context.
    shard: "database-throttled"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab-sidekiq-gitaly-throttled
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    shard: "gitaly-throttled"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab-sidekiq-imports
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    shard: "imports"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab-sidekiq-quarantine
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    shard: "quarantine"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab-sidekiq-urgent-cpu-bound
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    shard: "urgent-cpu-bound"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab-sidekiq-urgent-other
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    shard: "urgent-other"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab-sidekiq-urgent-authorized-projects
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    shard: "urgent-authorized-projects"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab-sidekiq-memory-bound
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    shard: "memory-bound"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab-sidekiq-elasticsearch
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    shard: "elasticsearch"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab-sidekiq-low-urgency-cpu-bound
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    shard: "low-urgency-cpu-bound"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab-sidekiq-catchall
  topic_prefix: pubsub-sidekiq-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sidekiq"
    shard: "catchall"
    stage: "main"
  # Exclude application.log since we already collect application_json.log
  remove_events:
    - key: "subcomponent"
      value: "^application$"
- name: gitlab_registry
  fluentd_tail_exclude_path: '["/var/log/containers/*pubsubbeat*.log"]'
  topic_prefix: pubsub-registry-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "registry"
    stage: "main"
- name: gitlab-cny_registry
  fluentd_tail_exclude_path: '["/var/log/containers/*pubsubbeat*.log"]'
  topic_prefix: pubsub-registry-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "registry"
    stage: "cny"
- name: mailroom
  fluentd_tail_exclude_path: '["/var/log/containers/*pubsubbeat*.log"]'
  topic_prefix: pubsub-mailroom-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "mailroom"
    stage: "main"
- name: pubsubbeat-pubsub
  topic_prefix: pubsub-pubsubbeat-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "pubsubbeat"
    stage: "main"
- name: jaeger
  fluentd_tail_exclude_path: '["/var/log/containers/*pubsubbeat*.log"]'
  topic_prefix: pubsub-jaeger-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "jaeger"
    stage: "main"
- name: fluentd
  fluentd_tail_exclude_path: '["/var/log/containers/*pubsubbeat*.log"]'
  topic_prefix: pubsub-fluentd-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "fluentd"
    stage: "main"
- name: gitlab-kas
  fluentd_tail_exclude_path: '["/var/log/containers/*pubsubbeat*.log"]'
  topic_prefix: pubsub-kas-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "kas"
    stage: "main"
- name: gitlab-cny-kas
  fluentd_tail_exclude_path: '["/var/log/containers/*pubsubbeat*.log"]'
  topic_prefix: pubsub-kas-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "kas"
    stage: "cny"
- name: thanos
  topic_prefix: pubsub-monitoring-inf
  elastic_key_prefix: "json."
  time_key: "ts"
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "thanos"
    stage: "main"
- name: prometheus
  topic_prefix: pubsub-monitoring-inf
  elastic_key_prefix: "json."
  time_key: "ts"
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "monitoring"
    stage: "main"
- name: alertmanager
  topic_prefix: pubsub-monitoring-inf
  elastic_key_prefix: "json."
  time_key: "ts"
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "monitoring"
    stage: "main"
- name: grafana
  topic_prefix: pubsub-monitoring-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "monitoring"
    stage: "main"
- name: consul
  topic_prefix: pubsub-consul-inf
  elastic_key_prefix: "json."
  time_key: "@timestamp"
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "consul"
    stage: "main"
- name: vault
  topic_prefix: pubsub-vault-inf
  elastic_key_prefix: "json."
  time_key: "@timestamp"
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    backend_path: '${record["path"]}'
    backend_type: '${record["type"]}'
    config: '${record["config"].is_a?(Hash) && record["config"].to_json || record["config"].to_s}'
    leader: '${record["leader"].is_a?(Hash) && record["leader"].to_json || record["leader"].to_s}'
    peer: '${record["peer"].is_a?(String) && { "ID" => record["peer"] } || record["peer"]}'
    type: "vault"
    stage: "main"
- name: sentry
  topic_prefix: pubsub-sentry-inf
  elastic_key_prefix: "json."
  time_key: "@timestamp"
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "sentry"
    stage: "main"

# Gitlab Pages Service
- name: pages
  pathMatch: "gitlab-gitlab-pages-*gitlab_gitlab-pages*"
  topic_prefix: pubsub-pages-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "web-pages"
    stage: "main"

- name: pages-cny
  pathMatch: "gitlab-cny-gitlab-pages*"
  topic_prefix: pubsub-pages-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "web-pages"
    stage: "cny"

- name: camoproxy
  fluentd_tail_exclude_path: '["/var/log/containers/*pubsubbeat*.log"]'
  topic_prefix: pubsub-camoproxy-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "camoproxy"
    stage: "main"

- name: redis-ratelimiting
  pathMatch: "redis-ratelimiting-*_redis_redis-*"
  topic_prefix: pubsub-redis-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "redis-ratelimiting"
    stage: "main"

- name: redis-ratelimiting-sentinel
  pathMatch: "redis-ratelimiting-*_redis_sentinel-*"
  topic_prefix: pubsub-redis-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "redis-ratelimiting"
    stage: "main"

- name: redis-registry-cache
  pathMatch: "redis-registry-cache-*_redis_redis-*"
  topic_prefix: pubsub-redis-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "redis-registry-cache"
    stage: "main"

- name: redis-registry-cache-sentinel
  pathMatch: "redis-registry-cache-*_redis_sentinel-*"
  topic_prefix: pubsub-redis-inf
  elastic_key_prefix: "json."
  keep_time_key: true
  logstashPrefix: ""
  custom_records:
    type: "redis-registry-cache"
    stage: "main"

{
  local defaults = {
    name: '',
    time_key: 'time',
    keep_time_key: false,
    reserve_time: true,
    reserve_data: false,
    remove_key_name_field: true,
    pathMatch: '*' + self.name + '*',
    time_format: '%Y-%m-%dT%H:%M:%S.%NZ',
    fluentd_tail_exclude_path: '[]',
    inject_key_prefix: '# no inject key prefix',
    remove_events: [],
  },

  local indexContainersInputConf = (import 'index.containers.input.conf.libsonnet'),

  createPubSubStanza(env, loggingContainers)::
    std.join('', [
      |||
        <match %(name)s.**>
          @type gcloud_pubsub
          topic %(topic_prefix)s-%(env)s
        </match>
      ||| % (container { env: env })
      for container in loggingContainers
    ]),

  createInputConfStanza(env, loggingContainers)::
    std.join('', [
      indexContainersInputConf.sourceConf % (container)
      + std.join('', [
        indexContainersInputConf.removeEventsConf % (event { name: container.name })
        for event in container.remove_events
      ])
      + indexContainersInputConf.filterConf % (container)
      + indexContainersInputConf.transformerConf % (
        {
          name: container.name,
          env: env,
          custom_records_string:
            std.join('\n    ', [
              '%(key)s %(value)s' % (record)
              for record in std.makeArray(
                std.length(container.custom_records),
                function(x) {
                  key: std.objectFields(container.custom_records)[x],
                  value: std.objectValues(container.custom_records)[x],
                }
              )
            ]),
        }
      )
      for container in std.makeArray(std.length(loggingContainers), function(x) defaults + loggingContainers[x])
    ])
    + indexContainersInputConf.kubeMetadataConf,
}

{
  sourceConf: |||
    <source>
      @id %(name)s.log
      @type tail
      path /var/log/containers/%(pathMatch)s.log
      exclude_path %(fluentd_tail_exclude_path)s
      pos_file /var/log/%(name)s.containers.log.pos
      tag raw.%(name)s.*
      read_from_head true
      <parse>
        @type multi_format
        <pattern>
          format json
          time_key %(time_key)s
          keep_time_key %(keep_time_key)s
          time_format %(time_format)s
        </pattern>
        <pattern>
          format /^(?<time>.+) (?<stream>stdout|stderr) [^ ]* (?<log>.*)$/
          time_format %%Y-%%m-%%dT%%H:%%M:%%S.%%N%%:z
        </pattern>
      </parse>
    </source>
  |||,

  filterConf: |||

    # Fixes json fields in Elasticsearch
    <filter raw.%(name)s.**>
      @id raw.%(name)s.filter_parser
      @type parser
      key_name log
      reserve_time %(reserve_time)s
      reserve_data %(reserve_data)s
      remove_key_name_field %(remove_key_name_field)s
      <parse>
        @type multi_format
        <pattern>
          format json
          time_key %(time_key)s
          keep_time_key %(keep_time_key)s
        </pattern>
        <pattern>
          format none
        </pattern>
      </parse>
    </filter>

    # Detect exceptions in the log output and forward them as one log entry.
    <match raw.%(name)s.**>
      @id %(name)s.kubernetes
      @type detect_exceptions
      remove_tag_prefix raw
      message message
      stream stream
      multiline_flush_interval 5
      force_line_breaks true
      max_bytes 500000
      max_lines 40000
    </match>
  |||,

  transformerConf: |||
    <filter %(name)s.**>
      @type record_transformer
      enable_ruby
      <record>
        environment %(env)s
        kubernetes.region ${ENV["K8S_NODE_NAME"]&.match(/-(us-\w+?-\w)-/)&.captures&.first || "us-east1"}
        params ${record["params"] && record["params"].each { |param| param["value"] = param["value"].to_s if param.is_a?(Hash) }}
        shard default
        tag ${tag}
        tier sv
        time ${record["time"] || time.utc.iso8601(3)}
        %(custom_records_string)s
      </record>
    </filter>
  |||,

  removeEventsConf: |||
    <filter %(name)s.**>
      @type grep
      <exclude>
        key %(key)s
        pattern /%(value)s/
      </exclude>
    </filter>
  |||,

  kubeMetadataConf: |||

    # Enriches records with Kubernetes metadata
    <filter **>
      @id filter_index_containers_kubernetes_metadata
      @type kubernetes_metadata
    </filter>
  |||,
}

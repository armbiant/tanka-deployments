local k = import 'k.libsonnet';
local parseYaml = std.native('parseYaml');

local secret = k.core.v1.secret;
local serviceAccount = k.core.v1.serviceAccount;
local configMap = k.core.v1.configMap;
local daemonSet = k.apps.v1.daemonSet;
local container = k.core.v1.container;
local envVar = k.core.v1.envVar;
local service = k.core.v1.service;
local serviceAccount = k.core.v1.serviceAccount;
local clusterRole = k.rbac.v1.clusterRole;
local clusterRoleBinding = k.rbac.v1.clusterRoleBinding;

local fluentdConfigGenerator = (import 'fluentd-generate-config.libsonnet');

{
  local defaults = {
    envName: 'dev',
    image: {
      repository: 'registry.ops.gitlab.net/gitlab-com/gl-infra/fluentd-docker',
      tag: 'v7.0.0',
    },
    imagePullSecrets: [],
    googleProject: '',
    labels: {},
    rbacRules: [
      {
        apiGroups: [''],
        resources: ['namespaces', 'pods'],
        verbs: ['get', 'watch', 'list'],
      },
    ],
    resources: {
      limits: {
        cpu: '500m',
        memory: '1G',
      },
      requests: {
        cpu: '100m',
        memory: '100M',
      },
    },
  },

  new(name='fluentd-elasticsearch', envValues={})::
    local values = defaults + envValues;

    local labels = values.labels.resources;
    local selector_labels = values.labels.selector;
    local deployment_labels = {
      type: 'logging',
      deployment: name,
      stage: 'main',
    };

    {
      serviceAccount:
        serviceAccount.new(name)
        + serviceAccount.metadata.withAnnotations(
          {
            'iam.gke.io/gcp-service-account': 'fluentd-kube@%s.iam.gserviceaccount.com' % values.googleProject,
          }
        )
        + serviceAccount.metadata.withLabels(labels),

      clusterRole:
        clusterRole.new(name)
        + clusterRole.withRules(values.rbacRules)
        + clusterRole.metadata.withLabels(labels),

      clusterRoleBinding:
        clusterRoleBinding.new(name)
        + clusterRoleBinding.bindRole(self.clusterRole)
        + clusterRoleBinding.withSubjects({
          kind: 'ServiceAccount',
          name: name,
          namespace: 'logging',
        })
        + clusterRoleBinding.metadata.withLabels(labels),

      local loggingContainers = parseYaml(importstr 'logging-config.yaml')[0],

      configMaps: {
        main:
          configMap.new(name + '-main')
          + configMap.metadata.withLabels(labels)
          + configMap.withData({
            'fluent.conf': (importstr 'fluent.conf'),
          }),
        config:
          configMap.new(name)
          + configMap.metadata.withLabels(labels)
          + configMap.withData({
            'monitoring.conf': (importstr 'monitoring.conf'),
            'pubsub.conf': fluentdConfigGenerator.createPubSubStanza(values.envName, loggingContainers),
            'system.conf': (importstr 'system.conf'),
            'index.containers.output.conf': '',
            'index.containers.input.conf': fluentdConfigGenerator.createInputConfStanza(values.envName, loggingContainers),
          }),
      },

      local configHash = std.md5(std.toString(self.configMaps)),

      daemonSet:
        daemonSet.new(
          name=name,
          containers=[
            container.new(
              name=name,
              image=std.join(':', [values.image.repository, values.image.tag]),
            )
            + container.withEnv([
              envVar.new('FLUENTD_CONF', '../../etc/fluent/fluent.conf'),
              envVar.new('FLUENTD_ARGS', '--no-supervisor -q'),
              envVar.new('LOGSTASH_PREFIX', 'logstash'),
              envVar.new('OUTPUT_HOST', '')
              + envVar.valueFrom.secretKeyRef.withName('%s-output' % name)
              + envVar.valueFrom.secretKeyRef.withKey('host'),
              envVar.new('OUTPUT_PORT', '9243'),
              envVar.new('OUTPUT_USER', '')
              + envVar.valueFrom.secretKeyRef.withName('%s-output' % name)
              + envVar.valueFrom.secretKeyRef.withKey('user'),
              envVar.new('OUTPUT_PASSWORD', '')
              + envVar.valueFrom.secretKeyRef.withName('%s-output' % name)
              + envVar.valueFrom.secretKeyRef.withKey('password'),
              envVar.new('OUTPUT_PATH', ''),
              envVar.new('OUTPUT_SCHEME', 'https'),
              envVar.new('OUTPUT_SSL_VERIFY', 'true'),
              envVar.new('OUTPUT_SSL_VERSION', 'TLSv1_2'),
              envVar.new('OUTPUT_TYPE_NAME', '_doc'),
              envVar.new('OUTPUT_BUFFER_CHUNK_LIMIT', '2M'),
              envVar.new('OUTPUT_BUFFER_QUEUE_LIMIT', '8'),
              envVar.new('OUTPUT_LOG_LEVEL', 'info'),
              envVar.new('OUTPUT_LOG_ES_400_REASON', 'true'),
              envVar.fromFieldPath('K8S_NODE_NAME', 'spec.nodeName'),
            ])
            + container.resources.withRequests(values.resources.requests)
            + container.resources.withLimits(values.resources.limits),
          ]
        )
        + daemonSet.spec.selector.withMatchLabels(selector_labels)
        + daemonSet.spec.template.metadata.withLabels(selector_labels + deployment_labels)
        + daemonSet.spec.template.metadata.withAnnotations({
          'checksum/config': std.toString(configHash),
          'cluster-autoscaler.kubernetes.io/safe-to-evict': 'true',
        })
        + daemonSet.hostVolumeMount('varlog', '/var/log', '/var/log')
        + daemonSet.hostVolumeMount('varlibdockercontainers', '/var/lib/docker/containers', '/var/lib/docker/containers', readOnly=true)
        + daemonSet.hostVolumeMount('libsystemddir', '/usr/lib64', '/usr/lib64', readOnly=true)
        + daemonSet.configVolumeMount(name + '-main', '/etc/fluent')
        + daemonSet.configVolumeMount(name, '/etc/fluent/config.d')
        + daemonSet.spec.template.spec.withImagePullSecrets([{ name: secret } for secret in values.imagePullSecrets])
        + daemonSet.spec.template.spec.withPriorityClassName('high')
        + daemonSet.spec.template.spec.withTerminationGracePeriodSeconds(30)
        + daemonSet.spec.template.spec.withServiceAccountName(name)
        + daemonSet.spec.template.spec.withTolerations([
          {
            effect: 'NoExecute',
            operator: 'Exists',
          },
          {
            effect: 'NoSchedule',
            operator: 'Exists',
          },
        ])
        + daemonSet.spec.template.spec.securityContext.withRunAsUser(0)
        + daemonSet.spec.template.spec.securityContext.withRunAsGroup(0)
        + daemonSet.metadata.withLabels(labels),

      service:
        service.new(
          name + '-metrics',
          selector=selector_labels,
          ports=[{ name: 'metrics', port: 24231, targetPort: 24231 }]
        )
        + service.metadata.withLabels(labels),

      serviceMonitor: {
        apiVersion: 'monitoring.coreos.com/v1',
        kind: 'ServiceMonitor',
        metadata: {
          name: name,
          labels: labels {
            'gitlab.com/prometheus-instance': 'prometheus-system',
          },
        },
        spec: {
          endpoints: [{
            honorLabels: true,
            interval: '10s',
            path: '/metrics',
            port: 'metrics',
          }],
          jobLabel: name,
          namespaceSelector: {
            matchNames: ['logging'],
          },
          selector: {
            matchLabels: selector_labels,
          },
        },
      },
    },
}

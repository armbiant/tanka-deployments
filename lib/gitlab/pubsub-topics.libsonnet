local defaultTopics = [
  'camoproxy',
  'consul',
  'fluentd',
  'jaeger',
  'kas',
  'mailroom',
  'monitoring',
  'pages',
  'pubsubbeat',
  'rails',
  'redis',
  'registry',
  'shell',
  'sidekiq',
  'workhorse',
];

{
  gprd: defaultTopics,
  gstg: defaultTopics,
  ops: defaultTopics + [
    'vault',
    'sentry',
  ],
  pre: defaultTopics + [
    'vault',
  ],
  dev: [
    'default',
  ],
}

local clusters = import 'clusters.libsonnet';
local externalSecrets = import 'external-secrets.libsonnet';
local k = import 'k.libsonnet';
local pubsub_topics = import 'pubsub-topics.libsonnet';

{
  // Creates a new namespace containing a "name" label, in the same way that
  // helm does. This might not be necessary but is good for consistency, and
  // avoids confusing when making label queries.
  namespace(name):: k.core.v1.namespace.new(name) + {
    metadata+: {
      labels+: {
        name: name,
      },
    },
  },

  // Wrapper for kausal.deployment.new that adds "deployment" labels to pods.
  // See withDeploymentPodLabel below for an explanation of why we do this.
  deployment(name, type, replicas, containers, podLabels={})::
    k.apps.v1.deployment.new(name, replicas, containers, podLabels)
    + self.withDeploymentPodLabel(type),

  // Adds a "deployment" label to pods, whose value is the name of the
  // deployment (or statefulset, or anything with a pod spec under
  // spec.template). We do this in order to generate container-group-level
  // metrics without having to perform gnarly promql joins, e.g.
  // https://dashboards.gitlab.net/d/git-kube-containers/git-kube-containers-detail?orgId=1&from=now-6h%2Fm&to=now%2Fm&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-sigma=2
  withDeploymentPodLabel(type, deployment=null)::
    {
      local resource = self,
      spec+: {
        template+: {
          metadata+: {
            labels+: {
              type: type,
              deployment: if deployment == null then resource.metadata.name else deployment,
            },
          },
        },
      },
    },

  managedCert(name, domains):: {
    apiVersion: 'networking.gke.io/v1',
    kind: 'ManagedCertificate',
    metadata: {
      name: name,
    },
    spec: {
      domains: domains,
    },
  },

  backendConfig(name, iapSecretName=null):: {
    apiVersion: 'cloud.google.com/v1',
    kind: 'BackendConfig',
    metadata: {
      name: name,
    },
    spec: {
      iap: {
        enabled: iapSecretName != null,
        oauthclientCredentials: {
          secretName: iapSecretName,
        },
      },
    },
  },

  externalURL(hostname, env, fqdn=false)::
    '%s.%s.gke.gitlab.net%s' % [hostname, env, if fqdn then '.' else ''],

  // Our GKE clusters have some node pools that are supposed to be reserved for
  // particular workloads, such as various sidekiq priority classes. These are
  // not tainted, so any workload can run on them. Until this is resolved
  // (https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10688), it
  // might be desirable to prevent large workloads from running on anything but
  // the default node pool.
  //
  // Mix into a Pod.spec, or a pod template (e.g. a
  // Deployment.spec.template.spec)
  withNodePoolAffinity(nodePool='default'):: {
    affinity+: {
      nodeAffinity: {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [{
            matchExpressions: [{
              key: 'type',
              operator: 'In',
              values: [nodePool],
            }],
          }],
        },
      },
    },
  },

  standardMetricLabels(type, stage, shard=null):: [
    {
      targetLabel: 'type',
      replacement: type,
    },
    {
      targetLabel: 'stage',
      replacement: stage,
    },
    {
      sourceLabels: ['__meta_kubernetes_pod_node_name'],
      targetLabel: 'node',
    },
  ] + (
    if shard == null then [] else [
      {
        targetLabel: 'shard',
        replacement: shard,
      },
    ]
  ),

  toBytes(sizeStr)::
    local validInput = std.native('regexMatch')('^\\d+[a-zA-Z]+$', sizeStr);
    assert validInput : '"%s" is not a valid byte size expression' % sizeStr;

    local scalar = std.parseInt(std.native('regexSubst')('[a-zA-Z]+', sizeStr, ''));
    local unit = std.native('regexSubst')('\\d+', sizeStr, '');
    if unit == 'Gi' then
      scalar * 1024 * 1024 * 1024
    else if unit == 'Mi' then
      scalar * 1024 * 1024
    else if unit == 'Ki' then
      scalar * 1024
    else if unit == 'B' then
      scalar
    else
      assert false : '"%s" is not a recognized unit' % unit;
      0,

  environment(appName, cluster, envData, resourceDefaults):: {
    local defaults = {
      createNamespace:: false,
      namespaceName:: 'default',
      externalSecretsRole:: '',
      secretStorePaths:: ['k8s'],
    },
    local data = defaults + envData,

    apiVersion: 'tanka.dev/v1alpha1',
    kind: 'Environment',
    metadata: {
      name: '%s/%s' % [appName, cluster.name],
    },
    spec: {
      apiServer: cluster.apiServer,
      namespace: data.namespaceName,
      injectLabels: true,
      resourceDefaults: resourceDefaults,
      expectVersions: {},
    },
    data: data {
      [if data.createNamespace then 'namespaceObj']:
        $.namespace(data.namespaceName),
      [if data.externalSecretsRole != '' then 'externalSecretsStores']: [
        externalSecrets.secretsStore(appName, data.namespaceName, data.externalSecretsRole, cluster, path)
        for path in data.secretStorePaths
      ],
    },
  },

  genGitlabEnvs(appName, envsData, resourceDefaults={}):: {
    ['%s/%s' % [appName, cluster]]: $.environment(
      appName,
      clusters[cluster],
      envsData[cluster],
      resourceDefaults,
    )
    for cluster in std.objectFields(envsData)
  },

  clusters:: {
    all()::
      [
        cluster
        for cluster in std.objectFields(clusters)
        if clusters[cluster].type != 'dev'
      ],

    regionals()::
      [
        cluster
        for cluster in std.objectFields(clusters)
        if clusters[cluster].type == 'regional'
      ],


    zonals()::
      [
        cluster
        for cluster in std.objectFields(clusters)
        if clusters[cluster].type == 'zonal'
      ],
  },
}

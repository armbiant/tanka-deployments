local k = import 'k.libsonnet';
local serviceAccount = k.core.v1.serviceAccount;
local clusterRoleBinding = k.rbac.v1.clusterRoleBinding;
local subject = k.rbac.v1.subject;

{
  secretStoreName(appName, path)::
    if path != 'k8s' then std.join('-', [appName, path]) else appName,

  k8sAppSecretKey(clusterName, namespaceName, appName, name)::
    '%s/%s/%s/%s' % [clusterName, namespaceName, appName, name],

  k8sNamespaceSecretKey(clusterName, namespaceName, name)::
    '%s/%s/%s' % [clusterName, namespaceName, name],

  k8sEnvSecretKey(env, name)::
    'env/%s/%s' % [env, name],

  k8sEnvNamespaceSecretKey(env, namespace, name)::
    'env/%s/ns/%s/%s' % [env, namespace, name],

  secretsStore(appName, namespace, vaultRole, cluster, path='k8s'):: {
    local serviceAccountName = '%s-secrets' % appName,

    serviceAccount:
      serviceAccount.new(serviceAccountName),

    clusterRoleBinding:
      clusterRoleBinding.new('%s-tk-auth-delegator' % serviceAccountName)
      + clusterRoleBinding.roleRef.withApiGroup('rbac.authorization.k8s.io')
      + clusterRoleBinding.roleRef.withKind('ClusterRole')
      + clusterRoleBinding.roleRef.withName('system:auth-delegator')
      + clusterRoleBinding.withSubjects([
        subject.withKind('ServiceAccount')
        + subject.withName(serviceAccountName)
        + subject.withNamespace(namespace),
      ]),

    secretsStore: {
      apiVersion: 'external-secrets.io/v1beta1',
      kind: 'SecretStore',
      metadata: {
        name: $.secretStoreName(appName, path),
      },
      spec: {
        provider: {
          vault: {
            server: cluster.vaultServer,
            path: path,
            version: 'v2',
            auth: {
              kubernetes: {
                mountPath: 'kubernetes/%s' % cluster.clusterName,
                role: vaultRole,
                serviceAccountRef: {
                  name: serviceAccountName,
                },
              },
            },
          },
        },
      },
    },
  },

  externalSecret(
    name,
    appName,
    namespace,
    clusterName,
    data,
    refreshInterval='1h',
    creationPolicy='Owner',
    deletionPolicy='Delete',
    annotations={},
    labels={},
    secretStorePath='k8s',
    secretType='Opaque',
    secretDataTemplate=null,
    secretAnnotations={},
    secretLabels={},
  ):: {
    apiVersion: 'external-secrets.io/v1beta1',
    kind: 'ExternalSecret',
    metadata: {
      name: name,
      annotations: annotations,
      labels: labels,
    },
    spec: {
      secretStoreRef: {
        name: $.secretStoreName(appName, secretStorePath),
        kind: 'SecretStore',
      },
      refreshInterval:
        // set the refresh internal only if at least one of the secrets doesn't have a version set
        if std.find(true, [!std.objectHas(data[secret], 'version') for secret in std.objectFields(data)]) != [] then
          refreshInterval
        else
          '0',
      target: {
        name: name,
        creationPolicy: creationPolicy,
        deletionPolicy: deletionPolicy,

        template: {
          type: secretType,
          metadata: {
            annotations: if secretAnnotations != {} then secretAnnotations else annotations,
            labels: if secretLabels != {} then secretLabels else labels,
          },
          [if secretDataTemplate != null then 'data']: secretDataTemplate,
        },
      },
      data: [
        {
          secretKey: secret,
          remoteRef: {
            key: data[secret].key,
            property: data[secret].property,
            [if std.objectHas(data[secret], 'version') then 'version']: std.toString(data[secret].version),
          },
        }
        for secret in std.objectFields(data)
      ],
    },
  },
}

FROM debian:bullseye

ENV USE_GKE_GCLOUD_AUTH_PLUGIN=True

WORKDIR /build

RUN apt-get update && apt-get install -y \
  apt-transport-https \
  ca-certificates \
  colordiff \
  curl \
  git \
  gnupg \
  less \
  make

COPY .tool-versions /build/
COPY ./bin/install_tools /build/

RUN /build/install_tools

RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
  curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - && \
  apt-get update -y && \
  apt-get install -y google-cloud-sdk google-cloud-sdk-gke-gcloud-auth-plugin
